﻿using _010_TiresShop.Models.Products;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace _010_TiresShop.Models.Fleet
{
    [DataContract]
    [KnownType("GetKnownTypes")]
    public class Install
    {
        [Key]
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int CurrentMilage { get; set; }

        [Required]
        [DataMember]
        public DateTime InstallDate { get; set; }

        [DataMember]
        public ICollection<Tire> Tires { get; set; }

        [DataMember]
        [Required]
        public Car Car { get; set; }

        [DataMember]
        [Required]
        public DateTime Created { get; set; }

        [DataMember]
        [Required]
        public DateTime Updated { get; set; }

        public Install()
        {
            Tires = new List<Tire>();
        }

        private static Type[] GetKnownTypes()
        {
            return new Type[] {
               typeof(List<Tire>),
               typeof(Car),

            };

        }
    }
}