﻿using _010_TiresShop.Models.Fleet;
using _010_TiresShop.Models.Helpers;
using _010_TiresShop.Models.Products.TireEnums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using System.Web;

namespace _010_TiresShop.Models.Products
{
    [DataContract]
    [KnownType(typeof(List<Install>))]
    public class Tire
    {
        [Key]
        [DataMember]
        public int Id { get; set; }

        [Required]
        [DataMember]
        public string SerialNumber { get; set; }

        [DataMember]
        public float? TreadDepth { get; set; }

        [Required]
        [DataMember]
        public string Name { get; set; }

        [Required]
        [DataMember]
        public decimal SelfCostUsd { get; set; }

        [Required]
        [DataMember]
        public decimal PriceUsd { get; set; }

        [NotMapped]
        [DataMember]
        public decimal SelfCostUAH
        {
            get
            {
                return (decimal)(ExchangeDataSingleton.Instance.UsdRate.sale * (double)this.SelfCostUsd);
            }
        }

        [NotMapped]
        [DataMember]
        public decimal PriceUAH
        {
            get
            {
                return (decimal)(ExchangeDataSingleton.Instance.UsdRate.sale * (double)this.PriceUsd);
            }
        }

        [Required]
        [DataMember]
        public decimal Discount { get; set; }

        [DataMember]
        public DateTime Created { get; set; }
        [DataMember]
        public DateTime Updated { get; set; }

        //specific properties:
        [DataMember]
        public decimal? Width { get; set; }

        [DataMember]
        public decimal? Profile { get; set; }

        [Required]
        [DataMember]
        public string BrandName { get; set; }

        [Required]
        [DataMember]
        public AutomobileTypes AutomobileType { get; set; }

        public UsageTypes UsageType {get; set;}

        [Required]
        [DataMember]
        public TireDiameter Diameter { get; set; }

        [Required]
        [DataMember]
        public TireSeasons Season { get; set; }

        [DataMember]
        public string CountryOrigin { get; set; }

        [DataMember]
        public string ModelName { get; set; }

        [DataMember]
        public int? YearManufactured { get; set; }

        [DataMember]
        public string SpeedIndex { get; set; }

        [DataMember]
        public int? LoadIndex { get; set; }

        public ICollection<Install> Installs { get; set; }

        public Tire()
        {
            Installs = new List<Install>();
        }

    }
}