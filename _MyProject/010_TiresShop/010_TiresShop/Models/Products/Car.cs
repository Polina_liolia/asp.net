﻿using _010_TiresShop.Models.Persons;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace _010_TiresShop.Models.Fleet
{

    [DataContract]
    [KnownType("GetKnownTypes")]
    public class Car
    {
        [Key]
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        [Required]
        public string LicensePlate { get; set; }

        [DataMember]
        public int AverageMilage { get; set; }

        [DataMember]
        [Required]
        public Client Client { get; set; }

        [DataMember]
        [Required]
        public DateTime Created { get; set; }

        [DataMember]
        [Required]
        public DateTime Updated { get; set; }

        [DataMember]
        public ICollection<Install> Installs { get; set; }

        public Car()
        {
            Installs = new List<Install>();
        }

        private static Type[] GetKnownTypes()
        {
            return new Type[] {
               typeof(List<Install>),
               typeof(Client)
            };

        }
    }
}