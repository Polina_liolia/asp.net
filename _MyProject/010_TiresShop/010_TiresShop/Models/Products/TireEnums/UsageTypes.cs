﻿namespace _010_TiresShop.Models.Products.TireEnums
{
    public enum UsageTypes
    {
        Drive, Trailer, Steer
    }
}