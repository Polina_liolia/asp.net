﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _010_TiresShop.Models.Products.TireEnums
{
    public enum TireSeasons
    {
        Winter, Summer, All
    }
}