﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace _010_TiresShop.Models.Helpers
{
    [DataContract]
    public class ExchangeRate
    {
        [DataMember]
        public string ccy { get; set; }
        [DataMember]
        public string base_ccy { get; set; }
        [DataMember]
        public double buy { get; set; }
        [DataMember]
        public double sale { get; set; }
    }
}