﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace _010_TiresShop.Models.Helpers
{
    public sealed class ExchangeDataSingleton
    {
        private static readonly Lazy<ExchangeDataSingleton> instance =
            new Lazy<ExchangeDataSingleton>(() => new ExchangeDataSingleton());
        private ExchangeDataSingleton()
        {
            UpdateExchangeRates();
        }
        public static ExchangeDataSingleton Instance
        {
            get
            {
                return instance.Value;
            }
        }

        private DateTime lastUpdated;
        private ExchangeRate usdRate;
        private ExchangeRate eurRate;
        private ExchangeRate rurRate;
        private ExchangeRate btcRate;

        public ExchangeRate UsdRate
        {
            get
            {
                if ((DateTime.Now - lastUpdated).TotalHours > 8)
                {
                    this.UpdateExchangeRates();
                }
                return this.usdRate;
            }
            private set
            {
                usdRate = value;
            }
        }
        public ExchangeRate EurRate
        {
            get
            {
                if ((DateTime.Now - lastUpdated).TotalHours > 8)
                {
                    this.UpdateExchangeRates();
                }
                return this.eurRate;
            }
            private set
            {
                eurRate = value;
            }
        }
        public ExchangeRate RurRate
        {
            get
            {
                if ((DateTime.Now - lastUpdated).TotalHours > 8)
                {
                    this.UpdateExchangeRates();
                }
                return this.rurRate;
            }
            private set
            {
                rurRate = value;
            }
        }
        public ExchangeRate BtcRate
        {
            get
            {
                if ((DateTime.Now - lastUpdated).TotalHours > 8)
                {
                    this.UpdateExchangeRates();
                }
                return this.btcRate;
            }
            private set
            {
                btcRate = value;
            }
        }

        private void UpdateExchangeRates()
        {
            lastUpdated = DateTime.Now;
            var client = new HttpClient();
            var uri = new Uri("https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5");
            try
            {
                Task<string> respStreamTask = client.GetStringAsync(uri);
                respStreamTask.Wait();
                string respStream = respStreamTask.Result;
                ExchangeRate[] rates = JsonConvert.DeserializeObject<ExchangeRate[]>(respStream);
                UsdRate = rates.Where(r => r.ccy == "USD").FirstOrDefault();
                EurRate = rates.Where(r => r.ccy == "EUR").FirstOrDefault();
                RurRate = rates.Where(r => r.ccy == "RUR").FirstOrDefault();
                BtcRate = rates.Where(r => r.ccy == "BTC").FirstOrDefault();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }


    }
}