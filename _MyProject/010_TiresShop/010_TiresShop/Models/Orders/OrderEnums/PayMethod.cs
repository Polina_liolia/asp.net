﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _010_TiresShop.Models.Orders.OrderEnums
{
    public enum PayMethod
    {
        Prepayment, PartialPrepayment, COD
    }
}