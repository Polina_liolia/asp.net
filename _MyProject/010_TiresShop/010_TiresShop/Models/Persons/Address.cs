﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace _010_TiresShop.Models.Persons
{
    [DataContract]
    [KnownType(typeof(Client))]
    public class Address
    {
        [Key]
        [DataMember]
        public int Id { get; set; }

        [Required]
        [DataMember]
        public string Country { get; set; }

        [Required]
        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string PostalCode { get; set; }

        [Required]
        [DataMember]
        public string Street { get; set; }

        [Required]
        [DataMember]
        public string Building { get; set; }

        [DataMember]
        public string Room { get; set; }

        [DataMember]
        public DateTime Created { get; set; }

        [DataMember]
        public DateTime Updated { get; set; }

    }
}