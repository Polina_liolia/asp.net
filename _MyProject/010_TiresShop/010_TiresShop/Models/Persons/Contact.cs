﻿using _010_TiresShop.Models.Persons;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace _010_TiresShop.Models
{
    [DataContract]
    [KnownType(typeof(ContactType))]
    public class Contact
    {
        [Key]
        [DataMember]
        public int Id { get; set; }

        [Required]
        [DataMember]
        public string Value { get; set; }

        [Required]
        [DataMember]
        public ContactType Type { get; set; }

        [DataMember]
        public DateTime Created { get; set; }

        [DataMember]
        public DateTime Updated { get; set; }
       
    }
}