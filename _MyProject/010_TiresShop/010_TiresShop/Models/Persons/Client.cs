﻿using _010_TiresShop.Models.Fleet;
using _010_TiresShop.Models.Orders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace _010_TiresShop.Models.Persons
{
    [DataContract]
    [KnownType("GetKnownTypes")]
    public class Client
    {
        [Key]
        [DataMember]
        public int Id { get; set; }

        [Required]
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string FatherName { get; set; }

        [DataMember]
        public DateTime Created { get; set; }

        [DataMember]
        public DateTime Updated { get; set; }

        [DataMember]
        public ICollection<Address> Addresses { get; set; }

        [DataMember]
        public ICollection<Contact> Contacts { get; set; }

        [DataMember]
        public ICollection<Order> Orders { get; set; }

        [DataMember]
        public ICollection<Car> Cars { get; set; }

        public Client()
        {
            Orders = new List<Order>();
            Addresses = new List<Address>();
            Contacts = new List<Contact>();
            Cars = new List<Car>();
        }

        private static Type[] GetKnownTypes()
        {
            return new Type[] {
               typeof(List<Client>),
               typeof(List<Address>),
               typeof(List<Order>),
               typeof(List<Car>)
            };
            
        }
    }
}