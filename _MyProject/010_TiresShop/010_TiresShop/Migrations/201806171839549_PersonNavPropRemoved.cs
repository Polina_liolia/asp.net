namespace _010_TiresShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PersonNavPropRemoved : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Addresses", name: "Person_Id", newName: "Client_Id");
            RenameColumn(table: "dbo.Contacts", name: "Person_Id", newName: "Client_Id");
            RenameIndex(table: "dbo.Addresses", name: "IX_Person_Id", newName: "IX_Client_Id");
            RenameIndex(table: "dbo.Contacts", name: "IX_Person_Id", newName: "IX_Client_Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Contacts", name: "IX_Client_Id", newName: "IX_Person_Id");
            RenameIndex(table: "dbo.Addresses", name: "IX_Client_Id", newName: "IX_Person_Id");
            RenameColumn(table: "dbo.Contacts", name: "Client_Id", newName: "Person_Id");
            RenameColumn(table: "dbo.Addresses", name: "Client_Id", newName: "Person_Id");
        }
    }
}
