namespace _010_TiresShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CarAndInstall : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LicensePlate = c.String(nullable: false),
                        AverageMilage = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Updated = c.DateTime(nullable: false),
                        Client_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.Client_Id, cascadeDelete: true)
                .Index(t => t.Client_Id);
            
            CreateTable(
                "dbo.Installs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CurrentMilage = c.Int(nullable: false),
                        InstallDate = c.DateTime(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Updated = c.DateTime(nullable: false),
                        Car_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cars", t => t.Car_Id, cascadeDelete: true)
                .Index(t => t.Car_Id);
            
            CreateTable(
                "dbo.InstallTires",
                c => new
                    {
                        Install_Id = c.Int(nullable: false),
                        Tire_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Install_Id, t.Tire_Id })
                .ForeignKey("dbo.Installs", t => t.Install_Id, cascadeDelete: true)
                .ForeignKey("dbo.Tires", t => t.Tire_Id, cascadeDelete: true)
                .Index(t => t.Install_Id)
                .Index(t => t.Tire_Id);
            
            AddColumn("dbo.Tires", "SerialNumber", c => c.String(nullable: false));
            AddColumn("dbo.Tires", "TreadDepth", c => c.Single());
            AddColumn("dbo.Tires", "UsageType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Installs", "Car_Id", "dbo.Cars");
            DropForeignKey("dbo.InstallTires", "Tire_Id", "dbo.Tires");
            DropForeignKey("dbo.InstallTires", "Install_Id", "dbo.Installs");
            DropForeignKey("dbo.Cars", "Client_Id", "dbo.Clients");
            DropIndex("dbo.InstallTires", new[] { "Tire_Id" });
            DropIndex("dbo.InstallTires", new[] { "Install_Id" });
            DropIndex("dbo.Installs", new[] { "Car_Id" });
            DropIndex("dbo.Cars", new[] { "Client_Id" });
            DropColumn("dbo.Tires", "UsageType");
            DropColumn("dbo.Tires", "TreadDepth");
            DropColumn("dbo.Tires", "SerialNumber");
            DropTable("dbo.InstallTires");
            DropTable("dbo.Installs");
            DropTable("dbo.Cars");
        }
    }
}
