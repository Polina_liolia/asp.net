﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using _010_TiresShop.Models;
using _010_TiresShop.Models.Products;

namespace _010_TiresShop.Controllers
{
    public class TiresController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Tires
        public IQueryable<Tire> GetTires()
        {
            return db.Tires;
        }

        // GET: api/Tires/5
        [ResponseType(typeof(Tire))]
        public async Task<IHttpActionResult> GetTire(int id)
        {
            Tire tire = await db.Tires.FindAsync(id);
            if (tire == null)
            {
                return NotFound();
            }

            return Ok(tire);
        }

        // PUT: api/Tires/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTire(int id, Tire tire)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tire.Id)
            {
                return BadRequest();
            }

            db.Entry(tire).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TireExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tires
        [ResponseType(typeof(Tire))]
        public async Task<IHttpActionResult> PostTire(Tire tire)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Tires.Add(tire);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = tire.Id }, tire);
        }

        // DELETE: api/Tires/5
        [ResponseType(typeof(Tire))]
        public async Task<IHttpActionResult> DeleteTire(int id)
        {
            Tire tire = await db.Tires.FindAsync(id);
            if (tire == null)
            {
                return NotFound();
            }

            db.Tires.Remove(tire);
            await db.SaveChangesAsync();

            return Ok(tire);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TireExists(int id)
        {
            return db.Tires.Count(e => e.Id == id) > 0;
        }
    }
}