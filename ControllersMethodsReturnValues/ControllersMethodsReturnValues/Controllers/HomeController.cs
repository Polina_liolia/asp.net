﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ControllersMethodsReturnValues.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ViewResult SomeMethod()
        {
            //ViewBag и ViewData обращаются к одному и тому же хранилищу
            ViewBag.Head = "ViewBag: Привет мир!"; //поместили
            ViewData["Head"] = "ViewData: Привет мир!"; //перетерли
            TempData["Head"] = "TempData: Привет мир!";
            
            return View();
        }
    }
}