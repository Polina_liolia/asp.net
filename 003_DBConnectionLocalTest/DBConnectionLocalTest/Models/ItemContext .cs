﻿using System.Data.Entity;

namespace DBConnectionLocalTest.Models
{
    public class ItemContext : DbContext
    {
        public ItemContext() : base("ItemContext")
        {

        }
        public DbSet<Item> Items { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
    }
}