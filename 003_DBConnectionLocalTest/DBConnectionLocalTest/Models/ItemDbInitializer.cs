﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DBConnectionLocalTest.Models
{
    public class ItemDbInitializer : DropCreateDatabaseAlways<ItemContext>
    {
        protected override void Seed(ItemContext db)
        {
            db.Items.Add(new Item() { Id = 1, Name = "Яблоки зеленые",Price=19.50f, Quantity=2});
            db.Items.Add(new Item() { Id = 2, Name = "Яблоки красные", Price = 12.50f, Quantity = 1 });
            db.Items.Add(new Item() { Id = 3, Name = "Капуста", Price = 17.50f, Quantity = 1 });
            base.Seed(db);
        }
    }
}