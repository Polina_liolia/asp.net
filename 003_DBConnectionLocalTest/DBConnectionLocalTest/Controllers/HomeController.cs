﻿using DBConnectionLocalTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DBConnectionLocalTest.Controllers
{
    public class HomeController : Controller
    {
        // создаем контекст данных
        ItemContext db = new ItemContext();

        public ActionResult Index()
        {
            // получаем из бд все объекты Item
            IEnumerable<Item> items = db.Items;
            // передаем все объекты в динамическое свойство Items в ViewBag
            ViewBag.Items = items;
            // возвращаем представление
            return View();
        }

        [HttpGet]
        public ActionResult Buy(int id)
        {
            ViewBag.Id = id;
            return View();
        }


        [HttpPost]
        public string Buy(Purchase purchase)
        {
            purchase.Date = DateTime.Now;
            // добавляем информацию о покупке в базу данных
            db.Purchases.Add(purchase);
            // сохраняем в бд все изменения
            db.SaveChanges();
            return "Спасибо," + purchase.Person + ", за покупку!";
        }
    }


}