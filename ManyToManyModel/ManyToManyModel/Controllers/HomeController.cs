﻿using ManyToManyModel.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManyToManyModel.Controllers
{
    public class HomeController : Controller
    {
        StudentsContext db = new StudentsContext();
        public ActionResult Index()
        {
            return View();
        }       

        public ActionResult IndexStudents()
        {
            var students = db.Students;//.Include("Courses");
            return View(students);
        }

        
        public ActionResult StudentDetales(int? id)
        {
            if (id == null)
                return HttpNotFound();
            Student student = db.Students.Find(id);
            if (student == null)
                return HttpNotFound();
            return View(student);
        }

        public ActionResult EditStudent(int id = 0)
        {
            Student student = db.Students.Find(id);
            if (student == null)
                return HttpNotFound();
            ViewBag.Courses = db.Courses.ToList<Course>();
            return View(student);
        }

        [HttpPost]
        public ActionResult EditStudent(Student student, int[] selectedCourses)
        {
            Student newStudent = db.Students.Find(student.Id);
            newStudent.Name = student.Name;
            newStudent.Surname = student.Surname;

            newStudent.Courses.Clear();
            if (selectedCourses != null)
            {
                //получаем выбранные курсы
                foreach (var c in db.Courses.Where(co => selectedCourses.Contains(co.Id)))
                {
                    newStudent.Courses.Add(c);
                }
            }

            db.Entry(newStudent).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult IndexCourses()
        {
            var courses = db.Courses;//.Include("Students");
            return View(courses);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

    }
}