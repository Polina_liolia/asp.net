﻿using ManyToManyModel.App_Start;
using ManyToManyModel.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ManyToManyModel
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            IDatabaseInitializer<StudentsContext> initializer = new StudentsDBInitializer();
            Database.SetInitializer(initializer);
            using (StudentsContext context = new StudentsContext())
            {
                initializer.InitializeDatabase(context);
            }
        }
    }
}
