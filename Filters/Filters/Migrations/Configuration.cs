namespace Filters.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Models;
    using Microsoft.AspNet.Identity.EntityFramework;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            if (context.Cars.Count() == 0)
            {
                context.Cars.Add(new Car("Mersedes"));
                context.Cars.Add(new Car("BMW"));
                context.Cars.Add(new Car("Seat"));
                context.Cars.Add(new Car("Fiat"));
                context.Cars.Add(new Car("Mazda"));
                context.Cars.Add(new Car("Toyota"));
            }
            
            if(context.Roles.Count() == 0)
            {
                context.Roles.Add(new IdentityRole() { Name = "Admin" });
                context.Roles.Add(new IdentityRole() { Name = "User" });
            }
           
            if(context.Users.Count() > 0)
            {
                IdentityRole adminRole = context.Roles.FirstOrDefault(r => r.Name == "Admin");
                if (adminRole != null && adminRole.Users.Count == 0)
                {
                    IdentityUser user = context.Users.First();
                    IdentityUserRole userRole = new IdentityUserRole() { RoleId = adminRole.Id, UserId = user.Id };
                    user.Roles.Add(userRole);
                }
                
            }

        }
    }
}
