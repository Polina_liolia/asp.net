﻿namespace Filters.Models
{
    public class Car
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Car()
        {

        }
        public Car(string name)
        {
            Name = name;
        }
    }
}