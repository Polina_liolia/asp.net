﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filters.MyFilters
{
    public class MyCompressAttribute : ActionFilterAttribute
    {
        /*
         С помощью заголовка Accept-Encoding в фильтре получаем, какой алгоритм сжатия 
         поддерживается браузером: deflate или gzip (нередко поддерживаются оба, тогда выбираем gzip). 
         Затем в зависимости от условий используем либо класс GZipStream, 
         либо DeflateStream для сжатия выходного потока.
        */
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpRequestBase request = filterContext.HttpContext.Request;
            //получаем заголовок Accept-Encoding, который указывает
            //какие алгоритмы сжатия он поддерживает
            string acceptEncoding = request.Headers["Accept-Encoding"];
            if (string.IsNullOrEmpty(acceptEncoding)) return;
            acceptEncoding = acceptEncoding.ToUpperInvariant();
            HttpResponseBase response = filterContext.HttpContext.Response;
            if (acceptEncoding.Contains("GZIP"))
            {
                response.AppendHeader("Content-encoding", "gzip");
                response.Filter = new GZipStream(response.Filter, CompressionMode.Compress);
            }
            else if (acceptEncoding.Contains("DEFLATE"))
            {
                response.AppendHeader("Content-encoding", "deflate");
                response.Filter = new DeflateStream(response.Filter, CompressionMode.Compress);
            }
        }
    }
}