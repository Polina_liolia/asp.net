﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filters.MyFilters
{
    public class MyAuthFilter : AuthorizeAttribute
    {
        private string[] allowedUsers;
        private string[] allowedRoles;

        public MyAuthFilter()
        {
         
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            //getting users from attr:
            if (base.Users.Length > 0)
            {
                allowedUsers = GetItemsArray(base.Users);
            }
            //getting roles from attr:
            if (base.Roles.Length > 0)
            {
                allowedRoles = GetItemsArray(base.Roles);
            }
            return httpContext.User.Identity.IsAuthenticated && HasRole(httpContext) && IsUserAllowed(httpContext);
        }

        private string[] GetItemsArray(string itemsString)
        {
            string[] items = itemsString.Split(new char[] { ',' });
            foreach (string item in items)
            {
                item.Trim();
            }
            return items;
        }

        private bool HasRole(HttpContextBase httpContext)
        {
            bool result = false;
            if (allowedRoles != null && allowedRoles.Length > 0)
            {
                foreach(string role in allowedRoles)
                {
                    if (httpContext.User.IsInRole(role))
                    {
                        result = true;
                        break;
                    }
                }
            }
            return result;
        }

        private bool IsUserAllowed(HttpContextBase httpContext)
        {
            bool result = false;
            if(allowedUsers != null && allowedUsers.Length > 0)
            {
                string userName = httpContext.User.Identity.Name;
                result = allowedUsers.Contains(userName);
            }
            return result;
        }
    }
}