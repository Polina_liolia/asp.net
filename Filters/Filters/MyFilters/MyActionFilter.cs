﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filters.MyFilters
{
    public class MyActionFilter : ActionFilterAttribute
    {
        //IActionFilter:
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string browserName = filterContext.HttpContext.Request.Browser.Browser;
            filterContext.HttpContext.Response.Write($"OnActionExecuting: Your browser is {browserName}<br>");
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {

            filterContext.HttpContext.Response.Write($"OnActionExecuted: Action completed!<br>");
        }

        //IResultFilter:
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            string userName = filterContext.HttpContext.User.Identity.Name;
            filterContext.HttpContext.Response.Write($"OnResultExecuting: user name {userName}<br>");
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            string requestTimeStamp = filterContext.HttpContext.Timestamp.ToShortTimeString();
            filterContext.HttpContext.Response.Write($"OnResultExecuted: request time {requestTimeStamp}<br>");
        }
    }
}