﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filters.MyFilters
{
    /*
     Данный фильтр во многом аналогичен встроенному фильтру кэширования OutputCache за тем исключением, 
     что в данном случае мы сами можем определить все параметры кэширования при различных условиях. 
     Например, определить разную логику кэширования для разных браузеров и т.д.
    */
    public class MyCacheFilter : ActionFilterAttribute
    {
        public int Duration { get; set; }

        public MyCacheFilter()
        {
            Duration = 3600; //default
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (Duration <= 0)
                return;
            HttpCachePolicyBase cache = filterContext.HttpContext.Response.Cache;
            TimeSpan cacheDuration = TimeSpan.FromSeconds(Duration);
            // задаем публичный кэш
            cache.SetCacheability(HttpCacheability.Public);
            // установка продолжительности кэширования
            cache.SetExpires(DateTime.Now.Add(cacheDuration));
            // установка параметра max-age
            cache.SetMaxAge(cacheDuration);
            // добавляем дополнительные параметры для кэширования
            cache.AppendCacheExtension("must-revalidate, proxy-revalidate");
        }
    }
}