﻿using System.Web;
using System.Web.Mvc;

namespace Filters
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new MyFilters.MyExceptionFilter());
            //filters.Add(new MyFilters.MyAuthFilter());
            filters.Add(new HandleErrorAttribute());
        }
    }
}
