﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Filters.MyFilters;

namespace Filters.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        // [HandleError]
        [MyExceptionFilter]
        public string GetException()
        {
            int[] array = new int[2] { 0, 1 };
            array[3] = 3; //IndexOutOfRangeException generating
            return "Exception!";
        }

        [HttpGet]
        //кэшировать вывод метода действия, чтобы полученный контент можно было в дальнейшем использовать повторно:
        [MyCacheFilter (Duration = 4000)]
        //[OutputCache(Duration = 360)] //стандартный фильтр кеширования
        [MyActionFilter]
        public string ActionFilterTest()
        {
            return $"Method returned: Tested<br>";
        }

        [MyWhitespacesFilter]
        public string WhitespacesString()
        {
            return "T h i s               s t r i n g          c o n t a i n s      a   l o t        o f       w h i t e s p a c e s ! Doesn't it?...";
        }

        [RequireHttps]// браузер перенаправит пользователя на то же действие, только с префиксом https.
        //Данный фильтр применяется только к GET-запросам.
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}