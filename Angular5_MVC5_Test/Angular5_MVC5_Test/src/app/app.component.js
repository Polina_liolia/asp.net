"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var linked_child_component_1 = require("./linked-child/linked-child.component");
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.userName = 'Tommy';
        this.age = 23;
        this.name = 'My first Angular5 + MVC5 app!!!';
        this.inputText = "Some sily string...";
        this.model = "Edit me different ways!";
        this.count = 5;
        this.condition = true;
        this.items = ["Apple", "Samsung", "LG"];
    }
    AppComponent.prototype.ngOnInit = function () {
        console.log("OnInit");
    };
    AppComponent.prototype.ngOnDestroy = function () {
        console.log("OnDestroy");
    };
    AppComponent.prototype.ngOnChanges = function (changes) {
        for (var propName in changes) {
            var chng = changes[propName];
            var cur = JSON.stringify(chng.currentValue);
            var prev = JSON.stringify(chng.previousValue);
            console.log(propName + ": currentValue = " + cur + ", previousValue = " + prev);
        }
    };
    //� ���� ������ �� ��� ����� �� ������������ ��������� ���������� � �������
    AppComponent.prototype.increment = function () { this.counterComponent.increment(); };
    AppComponent.prototype.decrement = function () { this.counterComponent.decrement(); };
    AppComponent.prototype.increase = function () {
        this.count++;
    };
    AppComponent.prototype.onChanged = function (increased) {
        increased == true ? this.count++ : this.count--;
    };
    AppComponent.prototype.changeCondition = function () {
        this.condition = !this.condition;
    };
    __decorate([
        core_1.ViewChild(linked_child_component_1.LinkedChildComponent),
        __metadata("design:type", linked_child_component_1.LinkedChildComponent)
    ], AppComponent.prototype, "counterComponent", void 0);
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
/*
ngOnChanges: ���������� �� ������ ngOnInit() ��� ��������� ��������� �������, ������� ������� ���������� ��������, � ����� ��� ����� �� ������������� ��� ��������� �� ��������.������ ����� � �������� ��������� ��������� ������ ������ SimpleChanges, ������� �������� ���������� � ������� �������� ��������.

ngOnInit: ���������� ���� ��� ����� ��������� ������� ����������, ������� ��������� � ��������.��������� ������������� ����������

ngDoCheck: ���������� ��� ������ �������� ��������� ������� ���������� ����� ����� ������� ngOnChanges � ngOnInit

ngAfterContentInit: ���������� ���� ��� ����� ������ ngDoCheck() ����� ������� ����������� � ������������� ���������� ���� html

ngAfterContentChecked: ���������� ����������� Angular ��� �������� ��������� �����������, ������� ����������� � ������������� ����������.���������� ����� ������ ngAfterContentInit() � � ����� ������� ������������ ������ ������ ngDoCheck().

ngAfterViewInit: ���������� ����������� Angular ����� ������������� ������������� ����������, � ����� ������������� �������� �����������.���������� ������ ���� ��� ����� ����� ������� ������ ������ ngAfterContentChecked()

ngAfterViewChecked: ���������� ����������� Angular ����� �������� �� ��������� � ������������� ����������, � ����� �������� ������������� �������� �����������.���������� ����� ������� ������ ������ ngAfterViewInit() � ����� ������� ������������ ������ ngAfterContentChecked()

ngOnDestroy: ���������� ����� ���, ��� ��������� Angular ������ ���������.

*/
//# sourceMappingURL=app.component.js.map