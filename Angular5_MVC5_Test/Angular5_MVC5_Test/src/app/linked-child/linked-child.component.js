"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var LinkedChildComponent = /** @class */ (function () {
    function LinkedChildComponent() {
        this.counter = 0;
        //object, used to pass event to parent:
        this.onChanged = new core_1.EventEmitter();
        //two-way event binding
        this.userNameChange = new core_1.EventEmitter();
        this.invisibility = false;
        this.hideShow = "Hide!";
    }
    LinkedChildComponent.prototype.ngOnInit = function () {
    };
    LinkedChildComponent.prototype.increment = function () { this.counter++; };
    LinkedChildComponent.prototype.decrement = function () { this.counter--; };
    Object.defineProperty(LinkedChildComponent.prototype, "userAge", {
        get: function () { return this._userAge; },
        set: function (age) {
            if (age < 0)
                this._userAge = 0;
            else if (age > 100)
                this._userAge = 100;
            else
                this._userAge = age;
        },
        enumerable: true,
        configurable: true
    });
    LinkedChildComponent.prototype.change = function (increased) {
        this.onChanged.emit(increased);
    };
    LinkedChildComponent.prototype.onNameChange = function (model) {
        this.userName = model;
        this.userNameChange.emit(model);
    };
    LinkedChildComponent.prototype.changeParentsMsg = function () {
        console.log(this.header);
        this.header.nativeElement.textContent = "Hell to world!";
    };
    LinkedChildComponent.prototype.hideButton = function () {
        this.hideShow = this.hideShow == "Hide!" ? "Show!" : "Hide!";
        this.invisibility = !this.invisibility;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], LinkedChildComponent.prototype, "userName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number),
        __metadata("design:paramtypes", [Number])
    ], LinkedChildComponent.prototype, "userAge", null);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], LinkedChildComponent.prototype, "onChanged", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], LinkedChildComponent.prototype, "userNameChange", void 0);
    __decorate([
        core_1.ContentChild("headerContent"),
        __metadata("design:type", core_1.ElementRef)
    ], LinkedChildComponent.prototype, "header", void 0);
    LinkedChildComponent = __decorate([
        core_1.Component({
            selector: 'app-linked-child',
            templateUrl: './linked-child.component.html',
            styleUrls: ['./linked-child.component.css']
        }),
        __metadata("design:paramtypes", [])
    ], LinkedChildComponent);
    return LinkedChildComponent;
}());
exports.LinkedChildComponent = LinkedChildComponent;
//# sourceMappingURL=linked-child.component.js.map