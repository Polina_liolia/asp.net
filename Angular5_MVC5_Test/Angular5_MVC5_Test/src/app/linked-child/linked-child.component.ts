import { Input, Component, OnInit, EventEmitter, Output, ContentChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-linked-child',
  templateUrl: './linked-child.component.html',
  styleUrls: ['./linked-child.component.css']
})
export class LinkedChildComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    }
    counter: number = 0;
    increment() { this.counter++; }
    decrement() { this.counter--; }

    @Input() userName: string;
    _userAge: number;
    @Input()
    set userAge(age: number) {
        if (age < 0)
            this._userAge = 0;
        else if (age > 100)
            this._userAge = 100;
        else
            this._userAge = age;
    }
    get userAge() { return this._userAge; }

    //object, used to pass event to parent:
    @Output() onChanged = new EventEmitter<boolean>();
    change(increased: any) {
        this.onChanged.emit(increased);
    }

    //two-way event binding
    @Output() userNameChange = new EventEmitter<string>();
    onNameChange(model: string) {
        this.userName = model;
        this.userNameChange.emit(model);
    }

    //getting directly passed content from parent
    @ContentChild("headerContent")
    header: ElementRef;

    changeParentsMsg() {
        console.log(this.header);
        this.header.nativeElement.textContent = "Hell to world!";
    }

    invisibility: boolean = false;
    hideShow: string = "Hide!";
    hideButton() {
        this.hideShow = this.hideShow == "Hide!" ? "Show!" : "Hide!";
        this.invisibility = !this.invisibility;

    }
}
