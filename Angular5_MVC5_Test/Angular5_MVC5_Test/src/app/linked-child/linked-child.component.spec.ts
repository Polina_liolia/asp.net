import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkedChildComponent } from './linked-child.component';

describe('LinkedChildComponent', () => {
  let component: LinkedChildComponent;
  let fixture: ComponentFixture<LinkedChildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkedChildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkedChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
