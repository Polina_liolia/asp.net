import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Directive({
    selector: '[bold]',
    //events holders binding:
    host: {
        '(mouseenter)': 'onMouseEnter()',
        '(mouseleave)': 'onMouseLeave()'
    }
})
export class BoldDirective implements OnInit {
    
    //passing data to directive
    @Input() selectedSize = "18px";
    @Input() defaultSize = "16px";

    constructor(private elementRef: ElementRef) {
        //this.elementRef.nativeElement.style.fontWeight = "bold";
        this.elementRef.nativeElement.style.cursor = "pointer";
        
    }

    ngOnInit(): void {
        this.setFontSize(this.defaultSize);
    }

    //alternative:
    //constructor(private elementRef: ElementRef, private renderer: Renderer2) {

    //    this.renderer.setStyle(this.elementRef.nativeElement, "font-weight", "bold");
    //}

    //interaction with user:
    //using @HostListener:
    //@HostListener("mouseenter") onMouseEnter() {
    //    this.setFontWeight("bold");
    //}

    //@HostListener("mouseleave") onMouseLeave() {
    //    this.setFontWeight("normal");
    //}

    //the same, using host declaration (upper):
    onMouseEnter() {
        this.setFontWeight("bold");
        this.setFontSize(this.selectedSize);
    }

    onMouseLeave() {
        this.setFontWeight("normal");
        this.setFontSize(this.defaultSize);
    }

    setFontWeight(value: string): any {
        this.elementRef.nativeElement.style.fontWeight = value;
    }

    setFontSize(value: string): any {
        this.elementRef.nativeElement.style.fontSize = value;
    }
}

