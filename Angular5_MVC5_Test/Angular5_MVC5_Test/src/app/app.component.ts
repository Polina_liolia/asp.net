import { Component, SimpleChanges, ViewChild } from '@angular/core';
import { OnInit, OnDestroy, OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { Console } from '@angular/core/src/console';
import { LinkedChildComponent } from './linked-child/linked-child.component';

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnDestroy, OnChanges{
    ngOnInit(): void {
        console.log("OnInit");
    }

    ngOnDestroy(): void {
        console.log("OnDestroy");
    }

    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            let chng = changes[propName];
            let cur = JSON.stringify(chng.currentValue);
            let prev = JSON.stringify(chng.previousValue);
            console.log(`${propName}: currentValue = ${cur}, previousValue = ${prev}`);
        }
    }
    // ��� �������� ����� ��������� ������ ��������� ����������
    @ViewChild(LinkedChildComponent)
    private counterComponent: LinkedChildComponent;

    //� ���� ������ �� ��� ����� �� ������������ ��������� ���������� � �������
    increment() { this.counterComponent.increment(); }
    decrement() { this.counterComponent.decrement(); }

    userName = 'Tommy';
    age = 23;
    name = 'My first Angular5 + MVC5 app!!!';
    inputText = "Some sily string...";
    model = "Edit me different ways!";
    count: number = 5;
    condition: boolean = true;
    items = ["Apple", "Samsung", "LG"];

    increase(): void {
        this.count++;
    }

    onChanged(increased: any) {
        increased == true ? this.count++ : this.count--; 
    }

    changeCondition(): void {
        this.condition = !this.condition;
    }
}

/*
ngOnChanges: ���������� �� ������ ngOnInit() ��� ��������� ��������� �������, ������� ������� ���������� ��������, � ����� ��� ����� �� ������������� ��� ��������� �� ��������.������ ����� � �������� ��������� ��������� ������ ������ SimpleChanges, ������� �������� ���������� � ������� �������� ��������.

ngOnInit: ���������� ���� ��� ����� ��������� ������� ����������, ������� ��������� � ��������.��������� ������������� ����������

ngDoCheck: ���������� ��� ������ �������� ��������� ������� ���������� ����� ����� ������� ngOnChanges � ngOnInit

ngAfterContentInit: ���������� ���� ��� ����� ������ ngDoCheck() ����� ������� ����������� � ������������� ���������� ���� html

ngAfterContentChecked: ���������� ����������� Angular ��� �������� ��������� �����������, ������� ����������� � ������������� ����������.���������� ����� ������ ngAfterContentInit() � � ����� ������� ������������ ������ ������ ngDoCheck().

ngAfterViewInit: ���������� ����������� Angular ����� ������������� ������������� ����������, � ����� ������������� �������� �����������.���������� ������ ���� ��� ����� ����� ������� ������ ������ ngAfterContentChecked()

ngAfterViewChecked: ���������� ����������� Angular ����� �������� �� ��������� � ������������� ����������, � ����� �������� ������������� �������� �����������.���������� ����� ������� ������ ������ ngAfterViewInit() � ����� ������� ������������ ������ ngAfterContentChecked()

ngOnDestroy: ���������� ����� ���, ��� ��������� Angular ������ ���������.

*/
