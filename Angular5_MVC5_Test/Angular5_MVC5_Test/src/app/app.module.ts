import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';


import { AppComponent }  from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ChildComponent } from './child/child.component';
import { LinkedChildComponent } from './linked-child/linked-child.component';
import { BoldDirective } from './bold.directive';



@NgModule({
    imports: [BrowserModule, FormsModule, /*RouterModule.forRoot(
        [
            { path: "", component: AppComponent }
        ]
    ), HttpClientModule, HttpModule*/ ],
  declarations: [ AppComponent, HomeComponent, AboutComponent, ChildComponent, LinkedChildComponent, BoldDirective],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
