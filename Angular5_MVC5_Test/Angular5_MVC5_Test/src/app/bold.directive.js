"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var BoldDirective = /** @class */ (function () {
    function BoldDirective(elementRef) {
        this.elementRef = elementRef;
        //passing data to directive
        this.selectedSize = "18px";
        this.defaultSize = "16px";
        //this.elementRef.nativeElement.style.fontWeight = "bold";
        this.elementRef.nativeElement.style.cursor = "pointer";
    }
    BoldDirective.prototype.ngOnInit = function () {
        this.setFontSize(this.defaultSize);
    };
    //alternative:
    //constructor(private elementRef: ElementRef, private renderer: Renderer2) {
    //    this.renderer.setStyle(this.elementRef.nativeElement, "font-weight", "bold");
    //}
    //interaction with user:
    //using @HostListener:
    //@HostListener("mouseenter") onMouseEnter() {
    //    this.setFontWeight("bold");
    //}
    //@HostListener("mouseleave") onMouseLeave() {
    //    this.setFontWeight("normal");
    //}
    //the same, using host declaration (upper):
    BoldDirective.prototype.onMouseEnter = function () {
        this.setFontWeight("bold");
        this.setFontSize(this.selectedSize);
    };
    BoldDirective.prototype.onMouseLeave = function () {
        this.setFontWeight("normal");
        this.setFontSize(this.defaultSize);
    };
    BoldDirective.prototype.setFontWeight = function (value) {
        this.elementRef.nativeElement.style.fontWeight = value;
    };
    BoldDirective.prototype.setFontSize = function (value) {
        this.elementRef.nativeElement.style.fontSize = value;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], BoldDirective.prototype, "selectedSize", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], BoldDirective.prototype, "defaultSize", void 0);
    BoldDirective = __decorate([
        core_1.Directive({
            selector: '[bold]',
            //events holders binding:
            host: {
                '(mouseenter)': 'onMouseEnter()',
                '(mouseleave)': 'onMouseLeave()'
            }
        }),
        __metadata("design:paramtypes", [core_1.ElementRef])
    ], BoldDirective);
    return BoldDirective;
}());
exports.BoldDirective = BoldDirective;
//# sourceMappingURL=bold.directive.js.map