﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace _009_Filtring.Models
{
    public class SoccerContext : DbContext
    {
        public SoccerContext() : base ("SoccerContextConnection")
        {}

        public DbSet<Player> Players { get; set; }
        public DbSet<Team> Teams { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Team>()
                .HasMany(t => t.Players)
                .WithRequired(p => p.Team);

            base.OnModelCreating(modelBuilder);
        }

        public System.Data.Entity.DbSet<_009_Filtring.Models.PlayersListViewModel> PlayersListViewModels { get; set; }
    }
}