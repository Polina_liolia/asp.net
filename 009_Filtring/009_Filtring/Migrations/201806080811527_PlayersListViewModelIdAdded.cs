namespace _009_Filtring.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PlayersListViewModelIdAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PlayersListViewModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Teams_DataGroupField = c.String(),
                        Teams_DataTextField = c.String(),
                        Teams_DataValueField = c.String(),
                        Positions_DataGroupField = c.String(),
                        Positions_DataTextField = c.String(),
                        Positions_DataValueField = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PlayersListViewModels");
        }
    }
}
