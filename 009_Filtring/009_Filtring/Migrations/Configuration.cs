namespace _009_Filtring.Migrations
{
    using _009_Filtring.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SoccerContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SoccerContext context)
        {
            if (context.Teams.Count() == 0 && context.Players.Count() == 0)
            {
                Team t1 = new Team() { Name = "��������" };
                Team t2 = new Team() { Name = "������" };
                Team t3 = new Team() { Name = "������" };
                context.Teams.Add(t1);
                context.Teams.Add(t2);
                context.Teams.Add(t3);
                Player p1 = new Player() { Name = "Name1", Age = 21, Position = "����������", Team = t1 };
                Player p2 = new Player() { Name = "Name2", Age = 22, Position = "������������", Team = t1 };
                Player p3 = new Player() { Name = "Name3", Age = 23, Position = "������������", Team = t1 };

                Player p4 = new Player() { Name = "Name14", Age = 21, Position = "����������", Team = t2 };
                Player p5 = new Player() { Name = "Name25", Age = 22, Position = "������������", Team = t2 };
                Player p6 = new Player() { Name = "Name36", Age = 23, Position = "������������", Team = t2 };

                Player p7 = new Player() { Name = "Name11", Age = 21, Position = "����������", Team = t3 };
                Player p8 = new Player() { Name = "Name21", Age = 22, Position = "������������", Team = t3 };
                Player p9 = new Player() { Name = "Name31", Age = 23, Position = "������������", Team = t3 };
                context.Players.AddRange(new List<Player> { p1, p2, p3, p4, p5, p6, p7, p8, p9 });

                context.SaveChanges();
            }
           
           
        }
    }
}
