namespace _014_BooksFiltringViewModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RequiredAttrsSet : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Books", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Categories", "Name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Categories", "Name", c => c.String());
            AlterColumn("dbo.Books", "Name", c => c.String());
        }
    }
}
