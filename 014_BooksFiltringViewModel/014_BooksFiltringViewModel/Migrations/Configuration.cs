namespace _014_BooksFiltringViewModel.Migrations
{
    using _014_BooksFiltringViewModel.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<_014_BooksFiltringViewModel.Models.BooksContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(_014_BooksFiltringViewModel.Models.BooksContext context)
        {
            Category c1 = new Category() { Name = "C++" };
            Category c2 = new Category() { Name = "C#" };
            if(context.Categories.Count() == 0)
            {
                context.Categories.AddRange(new Category[] { c1, c2 });
                context.SaveChanges();
            }

            Book b1 = new Book() { Name = "C++ 17", Price = 770, Category = c1 };
            Book b2 = new Book() { Name = "C++ STL", Price = 420, Category = c1 };
            Book b3 = new Book() { Name = "WP", Price = 220, Category = c2 };
            Book b4 = new Book() { Name = "WPF", Price = 520, Category = c2 };
            if(context.Books.Count() == 0)
            {
                context.Books.AddRange(new Book[] { b1, b2, b3, b4 });
                context.SaveChanges();
            }
        }
    }
}
