﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _014_BooksFiltringViewModel.Models
{
    public class Book
    {
        [Key]
        [HiddenInput]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public decimal Price { get; set; }

        public virtual Category Category { get; set; }
    }
}