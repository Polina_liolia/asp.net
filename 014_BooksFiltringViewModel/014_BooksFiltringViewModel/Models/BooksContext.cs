﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace _014_BooksFiltringViewModel.Models
{
    public class BooksContext : DbContext
    {
        public BooksContext() : base("BooksContextConnection")
        { }

        public DbSet<Book> Books { get; set; }
        public DbSet<Category> Categories { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>()
                .HasRequired(b => b.Category)
                .WithMany(c => c.Books);

            base.OnModelCreating(modelBuilder);


        }
    }
}