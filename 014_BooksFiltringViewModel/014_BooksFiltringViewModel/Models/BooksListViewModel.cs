﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _014_BooksFiltringViewModel.Models
{
    public class BooksListViewModel
    {
        public int Id { get; set; }
        public ICollection<Book> Books { get; set; }
        public SelectList Categories { get; set; }
    }
}