﻿using Forms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Forms.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            ViewBag.BookId = 1;
            return View();
        }

        [HttpPost]
        public string Buy(string[] countiresMulti)
        {
            string BookId = Request.Form["BookId"];
            string Name = Request.Form["Person"];
            string Address = Request.Form["Address"];
            string Enabled = Request.Form["Enable"];
            
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Order details:");
            sb.AppendLine($"Name: {Name}");
            sb.AppendLine($"Address: {Address}");
            sb.AppendLine($"Book id: {BookId}");
            sb.AppendLine($"Enable: {Enabled}");
            sb.AppendLine("Selected countries:");
            //select with multi selection:
            foreach (string c in countiresMulti)
                sb.AppendLine(c);
            return sb.ToString();
        }

        public ActionResult ShowPurchaseForm(int id)
        {
            using (BookStoreModel bookStoreModel = new BookStoreModel())
            {
                Purchase p = bookStoreModel.Purchases.Find(id);
                return View(p);
            }
                
        }
        
        [HttpGet]
        public ActionResult ShowBook(int id)
        {
            using (BookStoreModel bookStoreModel = new BookStoreModel())
            {
                return View(bookStoreModel.Books.Find(id));
            }

        }

        [HttpGet]
        public ActionResult EditBook(int id)
        {
            using (BookStoreModel bookStoreModel = new BookStoreModel())
            {
                return View(bookStoreModel.Books.Find(id));
            }

        }

        [HttpPost]
        public ActionResult EditBook(Book book)
        {
            using (BookStoreModel bookStoreModel = new BookStoreModel())
            {
                bookStoreModel.Entry(book).State = System.Data.Entity.EntityState.Modified;
                bookStoreModel.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}