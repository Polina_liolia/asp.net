﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Redirect_ErrorCodes.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About(int id)
        {
            if (id >3)
                //temporary redirect (code 302):
                return Redirect("/Home/Index");
            //return RedirectToRoute(new { controller = "Home", action = "Index" });
            //return RedirectToAction("Square", "Home", new { a = 10, h = 12 });    //with params
                
                // permanent redirect (code 301):
            //return RedirectPermanent("/Home/Index");
            //return RedirectToRoutePermanent(new { controller = "Home", action = "Index" });
            //return RedirectToActionPermanent("Square", "Home", new { a = 10, h = 12 });   //with params

            ViewBag.Message = "Your application description page.";
        
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Check(int id)
        {
            int age = id;
            if (age < 21)
            {
                //return new HttpStatusCodeResult(404);   //404
                return HttpNotFound();                   //404 - the same
                //return new HttpUnauthorizedResult();  //401

            }
            return View("Index");
        }
    }
}