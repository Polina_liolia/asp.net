﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _015_Areas.Areas.Shop.Models
{
    public class Item
    {
        public int Id { get; set; }
        [Display(Name = "Название книги")]
        public string Name { get; set; }
        [Display(Name = "Цена")]
        public double Price { get; set; }


    }
}