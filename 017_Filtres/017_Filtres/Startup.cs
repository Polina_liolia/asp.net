﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(_017_Filtres.Startup))]
namespace _017_Filtres
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
