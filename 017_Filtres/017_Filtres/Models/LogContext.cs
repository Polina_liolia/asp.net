﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace _017_Filtres.Models
{
    public class LogContext : DbContext
    {
        public DbSet<Log> Logs { get; set; }
    }
}