﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _017_Filtres.Models
{
    public class Log
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Ip { get; set; }
        public string Url { get; set; }
        public DateTime Date { get; set; }
    }
}