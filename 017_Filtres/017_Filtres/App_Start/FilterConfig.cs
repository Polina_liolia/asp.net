﻿using _017_Filtres.Filters;
using System.Web;
using System.Web.Mvc;

namespace _017_Filtres
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new LogAttribute());
        }
    }
}
