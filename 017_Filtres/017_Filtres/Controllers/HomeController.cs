﻿using _017_Filtres.Filters;
using _017_Filtres.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _017_Filtres.Controllers
{
    // [Authorize]
    [Log]
    public class HomeController : Controller
    {
        public string Index()
        {
            string result = "Вы не авторизованы";
            if (User.Identity.IsAuthenticated)
            {
                result = "Ваш логин: " + User.Identity.Name + " :" + User.Identity.AuthenticationType;
            }
            return result;
        }

        public ActionResult Log()
        {
            var logs = new List<Log>();
            using (LogContext db = new LogContext())
            {
                logs = db.Logs.ToList();
            }
            return View(logs);
        }


        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}