﻿using _06_HW_Divider.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _06_HW_Divider.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetResult([Bind(Include = "Number,Div")] Divider divider)
        {
            if(ModelState.IsValid)
            {
                divider.GetResult();
                return PartialView(divider);
            }
            else
            {
                ModelState.AddModelError("Result", "Can't calculate result: unexpected data.");
            }
            return PartialView(divider);
        }

        
    }
}