﻿// Value is the element to be validated, params is the array of name/value pairs of the parameters extracted from the HTML, element is the HTML element that the validator is attached to

//notEqualTo validation
$.validator.addMethod("notequalto", function (value, element, params) {
    return !(value === params);
});

//checkIsInteger validation
$.validator.addMethod("checkisinteger", function (value, element, params) {
    let result = false;
    let number = Number(value);
    if (number !== NaN && number % 1 === 0) {
        result = true;
    }
    return result;
});


$.validator.unobtrusive.adapters.add("notequalto", ["otherproperty"], function (options) {
    options.rules["notequalto"] = options.params.otherproperty;
    options.messages["notequalto"] = options.message;
});



$.validator.unobtrusive.adapters.add("checkisinteger", ["otherproperty"], function (options) {
    options.rules["checkisinteger"] = options.params;
    options.messages["checkisinteger"] = options.message;
});