﻿using _06_HW_Divider.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _06_HW_Divider.Models
{
    public class Divider
    {
        [Required]
        [Range(-2147483648, 2147483647, ErrorMessage ="Number is out of range")]
        [CheckIsInteger(ErrorMessage ="Input is not an integer")] //my custom attribute
        public int Number { get; set; }

        [Required]
        [Range(-2147483648, 2147483647, ErrorMessage = "Number is out of range")]
        [NotEqualTo(0, ErrorMessage = "Divider can't be zero")] //my custom attribute
        [CheckIsInteger(ErrorMessage = "Input is not an integer")] //my custom attribute
        public int Div { get; set; }

        public double Result { get; set; }

        public Divider()
        {
            Div = 1; //divider has not to be 0
        }
        public Divider(int number, int div)
        {
            Number = number;
            Div = div;
        }

        public double GetResult()
        {
            if (Div != 0)
            {
                Result = ((double)Number) / Div;
            }
            return Result;
        }
    }
}