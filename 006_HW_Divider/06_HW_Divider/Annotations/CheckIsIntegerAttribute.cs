﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _06_HW_Divider.Annotations
{
    //server-side validation
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class CheckIsIntegerAttribute : ValidationAttribute, IClientValidatable
    {
        public override bool IsValid(object value)
        {
            bool result = false; //model is invalid by default
            if(!(value is null))
            {
                try
                {
                    int number = Convert.ToInt32(value);
                    result = true; //convertion succeeded
                }
                catch(Exception ex) //convertion failed
                {
                    Debug.WriteLine(ex.Message);
                }
            }
            return result;
        }

        //client-side validation
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            string errorMessage = this.ErrorMessageString;

            // The value here is needed by the jQuery adapter
            ModelClientValidationRule checkIsIntegerRule = new ModelClientValidationRule();
            checkIsIntegerRule.ErrorMessage = errorMessage;
            checkIsIntegerRule.ValidationType = "checkisinteger"; // This is the name the jQuery adapter will use

            yield return checkIsIntegerRule;
        }
    }
}