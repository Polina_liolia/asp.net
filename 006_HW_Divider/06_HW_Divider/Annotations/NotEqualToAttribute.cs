﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _06_HW_Divider.Annotations
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class NotEqualToAttribute : ValidationAttribute, IClientValidatable
    {
        private int number;
        public NotEqualToAttribute(int number)
        {
            this.number = number; //number to compare value with
        }

        
        //server-side validation
        public override bool IsValid(object value)
        {
            bool result = false; //model is invalid by default
            if(!(value is null))
            {
                int modelVal = -1;
                try
                {
                    modelVal = Convert.ToInt32(value);
                    //model is valid if its value is not equal to number passed to attribute
                    if (!modelVal.Equals(this.number))
                        result = true; 
                }
                catch(Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
            return result;
        }

        //client-side validation
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            string errorMessage = this.ErrorMessageString;

            // The value here is needed by the jQuery adapter
            ModelClientValidationRule notEqualToRule = new ModelClientValidationRule();
            notEqualToRule.ErrorMessage = errorMessage;
            notEqualToRule.ValidationType = "notequalto"; // This is the name the jQuery adapter will use
            
            //"otherpropertyname" is the name of the jQuery parameter for the adapter, must be LOWERCASE!
            notEqualToRule.ValidationParameters.Add("otherproperty", this.number);

            yield return notEqualToRule;
        }
    }
}