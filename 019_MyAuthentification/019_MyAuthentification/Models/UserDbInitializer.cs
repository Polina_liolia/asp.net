﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace _019_MyAuthentification.Models
{
    public class UserDbInitializer : DropCreateDatabaseAlways<UserContext>
    {
        protected override void Seed(UserContext db)
        {
            Role admin = new Role { Name = "admin" };
            Role user = new Role { Name = "user" };
            db.Roles.Add(admin);
            db.Roles.Add(user);
            db.Users.Add(new User
            {
                Email = "user_admin@gmail.com",
                Password = "1234567",
                Role = admin
            });

            db.Users.Add(new User
            {
                Email = "user@gmail.com",
                Password = "1234567",
                Role = user
            });
            base.Seed(db);
        }
    }
}