﻿namespace _019_MyAuthentification.Models
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}