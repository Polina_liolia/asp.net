﻿using System.Web;
using System.Web.Mvc;

namespace _019_MyAuthentification
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
