﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _022_StudentsReviews.Models
{
    public class Student
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Group { get; set; }
        [Required]
        public virtual ApplicationUser User { get; set; }
        public virtual IList<Review> Reviews { get; set; }

        public Student()
        {
            Reviews = new List<Review>();
        }

        public Student(ApplicationUser user, string group)
        {
            User = user;
            Group = group;
            Reviews = new List<Review>();
        }


    }
}