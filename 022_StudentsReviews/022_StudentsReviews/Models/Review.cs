﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _022_StudentsReviews.Models
{
    public class Review
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Text { get; set; }

        [Required]
        public virtual Student Student { get; set; }
        [Required]
        public virtual Subject Subj { get; set; }

        public Review(string text, Student student, Subject subj)
        {
            Text = text;
            Student = student;
            Subj = subj;
        }
    }
}