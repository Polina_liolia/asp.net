namespace _022_StudentsReviews.Migrations
{
    using _022_StudentsReviews.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<_022_StudentsReviews.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(_022_StudentsReviews.Models.ApplicationDbContext context)
        {
            UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            if (!roleManager.RoleExists("Admin"))
            {
                var roleresult = roleManager.Create(new IdentityRole("Admin"));
            }
            if (!roleManager.RoleExists("User"))
            {
                var roleresult = roleManager.Create(new IdentityRole("User"));
            }

            ApplicationUser user = userManager.FindByName("Ivan Ivanov");
            if (user == null)
            {
                user = new ApplicationUser()
                {
                    UserName = "Ivan Ivanov",
                    Email = "ivanov@gmail.com",
                    EmailConfirmed = true
                };
                IdentityResult userResult = userManager.Create(user, "123456");
                if (userResult.Succeeded)
                {
                    var roleResult = userManager.AddToRole(user.Id, "Admin");
                }

            }


        }
    }
}
