﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(_022_StudentsReviews.Startup))]
namespace _022_StudentsReviews
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
