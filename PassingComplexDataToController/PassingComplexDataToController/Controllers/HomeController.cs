﻿using PassingComplexDataToController.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace PassingComplexDataToController.Controllers
{
    public class HomeController : Controller
    {
        BookStoreDataContext db = new BookStoreDataContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Array()
        {
            return View();
        }

        [HttpPost]
       public string Array(List<string> names)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string name in names)
                sb.Append($"{name}; ");

            return sb.ToString();
        }

        [HttpGet]
        public ActionResult EditBooks()
        {
            return View(db.Books.ToList());
        }

        [HttpPost]
        public ActionResult EditBooks(List<Book> books)
        {
            foreach(Book book in books)
            {
                db.Entry(book).State = System.Data.Entity.EntityState.Modified;
            }
            db.SaveChanges();
            return Redirect("EditBooks");
        }

        [HttpGet]
        public ActionResult TwoBooks(int? id)
        {
            if (id == null)
                return HttpNotFound();
            Book book = db.Books.Find(id);
            if (book == null)
                return HttpNotFound();
            return View(book);
        }

        [HttpPost]
        public ActionResult TwoBooks(Book book, Book myBook)
        {
            db.Entry(book).State = System.Data.Entity.EntityState.Modified;
            db.Books.Add(myBook);
            db.SaveChanges();
            return Redirect("EditBooks");
        }


        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }
    }
}