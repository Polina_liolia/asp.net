namespace PassingComplexDataToController.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class BookStoreDataContext : DbContext
    {
        public BookStoreDataContext()
            : base("name=BookStoreDataContext")
        {
        }

        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<Purchase> Purchases { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
