﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PassingComplexDataToController.Models
{
    public class BooksDBInitializer : DropCreateDatabaseAlways<BookStoreDataContext>
    {
        protected override void Seed(BookStoreDataContext context)
        {
            base.Seed(context);
            context.Books.Add(new Book() { Name = "One, two - buckle my shoe", Author = "Agatha Christie", Price = 150 });
            context.Books.Add(new Book() { Name = "Boy Soup", Author = "Alan Smith", Price = 210 });
            context.Books.Add(new Book() { Name = "Blue bird", Author = "Marko Tan", Price = 325 });
            context.Books.Add(new Book() { Name = "Three circles", Author = "Ben Flacke", Price = 75 });
            context.Books.Add(new Book() { Name = "White night", Author = "Andrew Bat", Price = 350 });

            context.Purchases.Add(new Purchase() { Person = "Mark", Address = "New street, 1", Date = DateTime.Now, BookId = 1 });
            context.Purchases.Add(new Purchase() { Person = "Anny", Address = "Old street, 3", Date = DateTime.Now, BookId = 2 });
            context.Purchases.Add(new Purchase() { Person = "Janny", Address = "Big street, 74", Date = DateTime.Now, BookId = 3 });
            context.Purchases.Add(new Purchase() { Person = "Solona", Address = "Green street, 15", Date = DateTime.Now, BookId = 4 });

            context.SaveChanges();
        }
    }
}