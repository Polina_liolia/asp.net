﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _008_AutoPagination.Models
{
    public class Phone
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}