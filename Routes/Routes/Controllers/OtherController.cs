﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Routes.Controllers
{
    public class OtherController : Controller
    {
        // GET: Other
        public string Index()
        {
            return "You called an Index method of OtherController";
        }
    }
}