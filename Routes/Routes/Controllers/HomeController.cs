﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Routes.Controllers
{
    [RoutePrefix("home")]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

       public string ShowPrefixPage()
        {
            return "This page was get via route with Ru prefix.";
        }

        public string Multiparam(int id = -1, string name = "")
        {
            //the other way to get parametres data:
            int ID = Convert.ToInt32(RouteData.Values["id"]);
            string Name = (string)RouteData.Values["name"];
            
            //other parts of the path:
            string controller = (string)RouteData.Values["controller"];
            string action = (string)RouteData.Values["action"];
            string other = (string)RouteData.Values["catchall"]; //all other parts of a path (has to be parsed if needed)
            return $"Your route: {controller}/{action}/{ID}/{Name}/{other}";
        }



        //[Route("{id:int}/{name=volga}")]  //with default value
        //[Route("~/lol/twit/{id:int}")]    //deactivates Home prefix (path example: http://localhost:4747/lol/twit/45)
        [Route("{id:int}/{name}")]
        public string Test(int id, string name)
        {
            return id.ToString() + ". " + name;
        }

        /*
            Attributes constraints:
            alpha: соответствует только алфавитным символам латинского алфавита. Например, {id:alpha}
            bool: соответствует логическлму значению. Например, {id:bool}
            datetime: соответствует значению DateTime. Например, {id:datetime}
            decimal: соответствует значению decimal. Например, {id:decimal}
            double: соответствует значению double. Например, {id:double}
            float: соответствует значению float. Например, {id:float}
            length: соответствует строке определенной длины, либо ее длина должна быть в определенном диапазоне. Например, {id:length(5)} или {id:length(5, 15)}
            long: соответствует значению long. Например, {id:long}
            max: соответствует значению int, которое не больше значения max. Например, {id:max(99)}. Аналогичным образом действует ограничение min, только оно указывает на минимально допустимое значение сегмента.
            maxlength: соответствует строке, длина которой не больше определенного значения. Например, {id:maxlength(20)}. Аналогичным образом работает ограничение minlength, указывая на минимально допустимую длину строки
            range: указывает на диапазон, в пределах которого должно находиться значение сегмента. Например, {id:range(5, 20)}
            regex: соответствует регулярному выражению. Например, {id:regex(^\d{3}-\d{3}-\d{4}$)}          
         */
    }
}