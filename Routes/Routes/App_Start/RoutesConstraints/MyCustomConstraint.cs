﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace Routes.App_Start.RoutesConstraints
{
    public class MyCustomConstraint : IRouteConstraint
    {
        private string uri;
        public MyCustomConstraint(string uri)
        {
            this.uri = uri;
        }

        //если запрашиваемый ресурс совпадает со значением свойства httpContext.Request.Url.AbsolutePath,
        //то запрос не будет сопоставляться с маршрутом
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            return !(uri == httpContext.Request.Url.AbsolutePath);
        }
    }
}