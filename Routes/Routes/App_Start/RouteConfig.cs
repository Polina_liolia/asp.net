﻿using Routes.App_Start.RoutesConstraints;
using Routes.RouteHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Routes
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //turning routing attributes on:
            routes.MapMvcAttributeRoutes(); 

            //route with custom handler:
            Route newRoute = new Route("{handlerName}/{controller}/{action}", new MyRouteHandler());
            routes.Add(newRoute);


            routes.MapRoute(
                name: "MultiparamedRoute",
                url: "{controller}/{action}/{id}/{name}",
                defaults: new { controller = "Home", action = "Multiparam"}
                );

            //route with "Ru" prefix
            //specific routes should go first:
            routes.MapRoute(name: "Default2", url: "Ru{controller}/{action}/{id}", defaults: new { id = UrlParameter.Optional });  

            routes.MapRoute(
                name: "Default3",
                url: "{controller}/{action}/{id}/{*catchall}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            //setting constraints using regexes:
            routes.MapRoute(
                name: "Default4",
                url: "{controller}/{action}/{id}/{*catchall}",
                defaults: new { controller = "Home", action = "Index" },
                constraints: new { controller = "^H.*", id = @"\d{2}", httpMethod = new HttpMethodConstraint("GET") }
            );

            //setting my custom constraint:
            routes.MapRoute(
                name: "Default5",
                url: "{controller}/{action}/{id}/{*catchall}",
                defaults: new { controller = "Home", action = "Index" },
                constraints: new { myConstraint = new MyCustomConstraint("/Home/Index/12") }
                );

            //the same with ignoring routes:
            routes.IgnoreRoute("Home/Index/12");


        }
    }
}
