﻿using Menu_HierarchicalData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Menu_HierarchicalData.Controllers
{
    public class HomeController : Controller
    {
        MenuContext db = new MenuContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Menu()
        {
            List<MenuItem> menuItems = db.MenuItems.ToList();
            return PartialView(menuItems);
        }
    }
}