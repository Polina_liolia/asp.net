namespace Menu_HierarchicalData.Migrations
{
    using Menu_HierarchicalData.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Menu_HierarchicalData.Models.MenuContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Menu_HierarchicalData.Models.MenuContext context)
        {
            List<MenuItem> menuItems = new List<MenuItem>
            {
                new MenuItem{Id=1, Header = "�������", Url = "/Home/Index", Order = 1},
                new MenuItem{Id=2, Header = "� �����", Url = "/Home/About", Order = 2},
                new MenuItem{Id=3, Header = "��������", Url = "/Home/Contact", Order = 3},
                new MenuItem{Id=4, Header = "���� ������� ������ 1", Url = "#", Order = 1, ParentId = 2},
                new MenuItem{Id=5, Header = "���� ������� ������ 2", Url = "#", Order = 2, ParentId = 2},
                new MenuItem{Id=6, Header = "���� ������� ������ 3", Url = "#", Order = 3, ParentId = 2},
                new MenuItem{Id=7, Header = "���� ������� ������ 1", Url = "#",  Order = 1, ParentId = 4},
                new MenuItem{Id=8, Header = "���� ������� ������ 2", Url = "#", Order = 2, ParentId = 4},
                new MenuItem{Id=9, Header = "���� ������� ������ 3", Url = "#", Order = 3, ParentId = 4}
            };
            context.MenuItems.AddRange(menuItems);
            context.SaveChanges();
        }
    }
}
