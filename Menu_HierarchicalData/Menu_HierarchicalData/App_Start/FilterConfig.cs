﻿using System.Web;
using System.Web.Mvc;

namespace Menu_HierarchicalData
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
