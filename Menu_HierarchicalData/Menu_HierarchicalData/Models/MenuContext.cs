﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Menu_HierarchicalData.Models
{
    public class MenuContext : DbContext
    {
        public MenuContext() : base("MenuConnection")
        {

        }

        public DbSet<MenuItem> MenuItems { get; set; }

    }
}