﻿using SoccerComplexModel.App_Start;
using SoccerComplexModel.Models.Soccer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SoccerComplexModel
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            IDatabaseInitializer<SoccerDataModel> dbInitializer = new DropCreateSoccerModel();
            Database.SetInitializer(dbInitializer);
            using (SoccerDataModel db = new SoccerDataModel())
            {
                dbInitializer.InitializeDatabase(db);
            }
         
        }
    }
}
