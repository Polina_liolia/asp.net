﻿using SoccerComplexModel.Models.Soccer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SoccerComplexModel.App_Start

{
    public class DropCreateSoccerModel : DropCreateDatabaseIfModelChanges<SoccerDataModel>
    {
        protected override void Seed(SoccerDataModel context)
        {
            base.Seed(context);
            Team t1 = context.Teams.Add(new Team() { Name = "Dream", Coach = "Ivanov" });
            Team t2 = context.Teams.Add(new Team() { Name = "Team", Coach = "Vasiliev" });

            context.Players.Add(new Player() { Name = "Anton", Age = 25, Position = "First", Team = t1 });
            context.Players.Add(new Player() { Name = "Petr", Age = 27, Position = "Second", Team = t1 });
            context.Players.Add(new Player() { Name = "Vasia", Age = 22, Position = "Third", Team = t1 });
            context.Players.Add(new Player() { Name = "Sasha", Age = 21, Position = "First", Team = t2 });
            context.Players.Add(new Player() { Name = "Ivan", Age = 26, Position = "Second", Team = t2 });
            context.Players.Add(new Player() { Name = "Kirill", Age = 24, Position = "Third", Team = t2 });

            context.SaveChanges();
        }
    }
}