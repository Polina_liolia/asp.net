﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace SoccerComplexModel.Models.Soccer.Configurations
{
    public class PlayerConfigs : EntityTypeConfiguration<Player>
    {
        public PlayerConfigs()
        {
            HasKey(p => p.Id);
            HasRequired(p => p.Team);
        }
    }
}