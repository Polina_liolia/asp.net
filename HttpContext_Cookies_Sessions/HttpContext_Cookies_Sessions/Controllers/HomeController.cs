﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HttpContext_Cookies_Sessions.Controllers
{
    public class HomeController : Controller
    {
        public string Index()
        {
            //устанавливаем имя сессии:
            Session["name"] = "Polina's session";

            //установим кодировку ответа:
            //HttpContext.Response.Charset = "iso-8859-2";
            HttpContext.Response.Write("<h1>Hello World</h1>"); //может заменить возврат разметки представления

            //установка Cookies:
            HttpContext.Response.Cookies["id"].Value = "ca-4353w";
            HttpContext.Response.Cookies["tasty_cookie"].Value = "yammy";

            //getting all cookies:
            if (HttpContext.Request.Cookies.Count > 0)
            {
                HttpContext.Response.Write("<h2>Cookies:</h2>");
                foreach (string cookieName in HttpContext.Request.Cookies)
                {
                    HttpCookie cookie = HttpContext.Request.Cookies[cookieName];
                    HttpContext.Response.Write($"<p>{cookieName}: {cookie.Value}</p>");

                }
                HttpContext.Response.Write("<hr>");
            }


            bool isAdmin = HttpContext.User.IsInRole("admin"); // определяем, принадлежит ли пользователь к администраторам
            bool isAuth = HttpContext.User.Identity.IsAuthenticated; // аутентифицирован ли пользователь
            string login = HttpContext.User.Identity.Name; // логин авторизованного пользователя
            string browser = HttpContext.Request.Browser.Browser; //Получение браузера пользователя
            string user_agent = HttpContext.Request.UserAgent;  //братиться к агенту пользователя
            string url = HttpContext.Request.RawUrl;    //Получение url запроса
            string ip = HttpContext.Request.UserHostAddress;    //Получение IP-адреса пользователя
            string referrer = HttpContext.Request.UrlReferrer == null ? "" : HttpContext.Request.UrlReferrer.AbsoluteUri; //Получение реферера
            return "<p>Is admin: " + isAdmin +
                "</p><p>Authorized user: " + isAuth +
                "</p><p>User login: " + login +
                "</p><p>Browser: " + browser +
                "</p><p>User-Agent: " + user_agent +
                "</p><p>Url запроса: " + url +
                "</p><p>Реферер: " + referrer +
                "</p><p>IP-адрес: " + ip + "</p>";
        }

        public string GetSessionName()
        {
            var val = Session["name"];
            Session["name"] = null; //удалить значение сессии для ключа name
            return val.ToString();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}