﻿using System.Web;
using System.Web.Mvc;

namespace HttpContext_Cookies_Sessions
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
