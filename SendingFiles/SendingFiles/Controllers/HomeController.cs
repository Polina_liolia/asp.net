﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SendingFiles.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        //FileContentResult: отправляет клиенту массив байтов, считанный из файла
        public FileResult GetFile()
        {
            // Путь к файлу
            string file_path = Server.MapPath("~/Files/Exam.pdf");
            // Тип файла - content-type
            string file_type = "application/pdf";
            // Имя файла - необязательно
            string file_name = "Exam.pdf";
            return File(file_path, file_type, file_name);
        }

        //FilePathResult: представляет простую отправку файла напрямую с сервера
        // Отправка массива байтов
        public FileResult GetBytes()
        {
            string path = Server.MapPath("~/Files/Exam.pdf");
            byte[] mas = System.IO.File.ReadAllBytes(path);
            string file_type = "application/pdf";
            string file_name = "Exam.pdf";
            return File(mas, file_type, file_name);
        }

        //FileStreamResult: данный класс создает поток - объект System.IO.Stream, с помощью которого считывает и отправляет файл клиенту
        // Отправка потока
        public FileResult GetStream()
        {
            string path = Server.MapPath("~/Files/Exam.pdf");
            // Объект Stream
            FileStream fs = new FileStream(path, FileMode.Open);
            string file_type = "application/pdf";
            string file_name = "Exam.pdf";
            return File(fs, file_type, file_name);
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}