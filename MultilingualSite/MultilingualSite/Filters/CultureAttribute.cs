﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace MultilingualSite.Filters
{
    public class CultureAttribute : FilterAttribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            List<string> culturesAvailable = new List<string>() { "en", "ru" };
            string cultureName = string.Empty;
            HttpCookie langCookie = filterContext.HttpContext.Request.Cookies["lang"];
            //
            if (langCookie != null)
            {
                cultureName = langCookie.Value;
            }
            else
            {
                cultureName = "ru";
            }
            //chacking if we have such culture in the list:
            if(!culturesAvailable.Contains(cultureName))
            {
                cultureName = "ru";
            }
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureName);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(cultureName);

        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //no emplementation
        }
    }
}