﻿using MultilingualSite.Filters;
using MultilingualSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;

using System.Web;
using System.Web.Mvc;

namespace MultilingualSite.Controllers
{
    [Culture]
    public class HomeController : Controller
    {
        Context db = new Context();
        public ActionResult Index()
        {
            return View(db.Persons);
        }

        public ActionResult ChangeCulture(string lang)
        {
            //initial url:
            string returnUrl = Request.UrlReferrer.AbsolutePath;
            // Список культур
            List<string> cultures = new List<string>() { "ru", "en" };
            if (!cultures.Contains(lang))
            {
                lang = "ru";
            }
            // Сохраняем выбранную культуру в куки
            HttpCookie cookie = Request.Cookies["lang"];
            if (cookie != null)
                cookie.Value = lang;   // если куки уже установлено, то обновляем значение
            else
            {

                cookie = new HttpCookie("lang");
                cookie.HttpOnly = false;
                cookie.Value = lang;
                cookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(cookie);
            //going back to initial url:
            return Redirect(returnUrl);
        }
    }
}