namespace MultilingualSite.Migrations
{
    using MultilingualSite.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MultilingualSite.Models.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "MultilingualSite.Models.Context";
        }

        protected override void Seed(MultilingualSite.Models.Context context)
        {
            if(context.Persons.Count() == 0)
            {
                context.Persons.AddRange(
               new Person[] {
                    new Person(){Name = "Vasia", State = "active"},
                    new Person(){Name = "Petia", State = "active"}
               });
            }
        }
    }
}
