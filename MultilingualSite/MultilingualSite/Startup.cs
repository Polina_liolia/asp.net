﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MultilingualSite.Startup))]
namespace MultilingualSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
