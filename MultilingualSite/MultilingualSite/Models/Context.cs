﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MultilingualSite.Models
{
    public class Context : DbContext
    {
        public DbSet<Person> Persons { get; set; }
        public Context() : base("PersonDBHome")
        {

        }
    }
}