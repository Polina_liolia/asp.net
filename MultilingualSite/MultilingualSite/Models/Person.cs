﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MultilingualSite.Models
{
    public class Person
    {
        public int Id { get; set; }

        /*
         есть некоторое пространство имен ресурсов Resources, в нем определен тип Resource. 
         А имя ресурса представляет уже конкретный ресурс, значение которого будет использоваться в 
         качестве сообщения об ошибке при валидации
        */
        [Required(ErrorMessageResourceType = typeof(Resources.Resource), //в данном случае атрибут Required указывает на тип ресурса, который будет применяться
                  ErrorMessageResourceName = "NameRequired")] //имя ресурса 
        [Display(Name = "Name", ResourceType = typeof(Resources.Resource))] //свойство Name указывает на имя ресурса, который будет использоваться в качестве названия для соответствующего поля
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                  ErrorMessageResourceName = "CountryRequired")]
        [Display(Name = "Country", ResourceType = typeof(Resources.Resource))]
        public string State { get; set; }
    }
}