﻿using System.Web;
using System.Web.Mvc;

namespace _007_HTTPContext_JQuery_Filters
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
