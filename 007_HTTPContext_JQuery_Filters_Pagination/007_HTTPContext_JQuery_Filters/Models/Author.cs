﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _007_HTTPContext_JQuery_Filters.Models
{
    public class Author
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<BookComplex> Books { get; set; }
    }
}