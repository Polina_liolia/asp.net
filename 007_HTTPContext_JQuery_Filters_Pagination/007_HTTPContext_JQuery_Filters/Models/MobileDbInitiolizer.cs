﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace _007_HTTPContext_JQuery_Filters.Models
{
    public class MobileDbInitiolizer : DropCreateDatabaseAlways<MobileContext>
    {
        protected override void Seed(MobileContext context)
        {
            context.Phones.Add(new Phone { Id = 1, Model = "Samsung Galaxy III", Producer = "Samsung" });
            context.Phones.Add(new Phone { Id = 2, Model = "Samsung Ace II", Producer = "Samsung" });
            context.Phones.Add(new Phone { Id = 3, Model = "HTC Hero", Producer = "HTC" });
            context.Phones.Add(new Phone { Id = 4, Model = "HTC One S", Producer = "HTC" });
            context.Phones.Add(new Phone { Id = 5, Model = "HTC One X", Producer = "HTC" });
            context.Phones.Add(new Phone { Id = 6, Model = "LG Optimus 3D", Producer = "LG" });
            context.Phones.Add(new Phone { Id = 7, Model = "Nokia N9", Producer = "Nokia" });
            context.Phones.Add(new Phone { Id = 8, Model = "Samsung Galaxy Nexus", Producer = "Samsung" });
            context.Phones.Add(new Phone { Id = 9, Model = "Sony Xperia X10", Producer = "SONY" });
            context.Phones.Add(new Phone { Id = 10, Model = "Samsung Galaxy II", Producer = "Samsung" });
        }
    }
}