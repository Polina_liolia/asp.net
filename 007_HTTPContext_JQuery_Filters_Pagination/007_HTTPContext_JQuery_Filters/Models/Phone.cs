﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _007_HTTPContext_JQuery_Filters.Models
{
    public class Phone
    {
        public int Id { get; set; }
        public string Model { get; set; }
        public string Producer { get; set; }
    }
}