﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _007_HTTPContext_JQuery_Filters.Models
{
    public class IndexViewModel
    {
        public IEnumerable<Phone> Phones { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}