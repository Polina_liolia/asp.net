﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _007_HTTPContext_JQuery_Filters.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Session["name"] = "Polina";
            HttpContext.Response.Cookies["pasport"].Value = "MK232323"; //set cookie
            return View();
        }

        public string GetContext()
        {
            string browser = HttpContext.Request.Browser.Browser;
            string user_agent = HttpContext.Request.UserAgent;
            string url = HttpContext.Request.RawUrl;
            string ip = HttpContext.Request.UserHostAddress;
            //если referrer == null - пользователь набрал адрес сайта в строке браузера
            string referrer = HttpContext.Request.UrlReferrer == null ? "" : HttpContext.Request.UrlReferrer.AbsoluteUri;
            return "<p>Browser: " + browser + "</p><p>User-Agent: " + user_agent + "</p><p>Url запроса: " + url +
                "</p><p>Реферер: " + referrer + "</p><p>IP-адрес: " + ip + "</p>";
        }

        public void VoidContext()
        {
            Response.Write("Наш ответ global");
            string browser = HttpContext.Request.Browser.Browser;
            string user_agent = HttpContext.Request.UserAgent;
            string url = HttpContext.Request.RawUrl;
            string ip = HttpContext.Request.UserHostAddress;
            string referrer = HttpContext.Request.UrlReferrer == null ? "" : HttpContext.Request.UrlReferrer.AbsoluteUri;

            HttpContext.Response.Write(
                "<p>Browser: " + browser + "</p><p>User-Agent: " + user_agent + "</p><p>Url запроса: " + url +
                "</p><p>Реферер: " + referrer + "</p><p>IP-адрес: " + ip + "</p>");
        }

        public string GetCookies()
        {
            string c = HttpContext.Request.Cookies["pasport"].Value;
            return c.ToString();
        }

        public string GetDataFromSession()
        {
            string c = HttpContext.Request.Cookies["pasport"].Value + " " + Session["name"];
            return c.ToString();
        }
    }
}