﻿using _007_HTTPContext_JQuery_Filters.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _007_HTTPContext_JQuery_Filters.Controllers
{
    public class PaginationController : Controller
    {
        MobileContext db = new MobileContext();
        public ActionResult Index(int page = 1)
        {
            int pageSize = 3; // количество объектов на страницу
            IEnumerable<Phone> phonesPerPages = db.Phones.OrderBy(x => x.Id).
                Skip((page - 1) * pageSize).
                Take(pageSize).ToList();
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = db.Phones.Count() };
            IndexViewModel ivm = new IndexViewModel { PageInfo = pageInfo, Phones = phonesPerPages };
            return View(ivm);
        }
    }
}