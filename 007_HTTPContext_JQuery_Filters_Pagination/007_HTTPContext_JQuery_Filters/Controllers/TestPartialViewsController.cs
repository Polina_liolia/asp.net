﻿using _007_HTTPContext_JQuery_Filters.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _007_HTTPContext_JQuery_Filters.Controllers
{
    public class TestPartialViewsController : Controller
    {
        // GET: TestPartialViews
        public ActionResult Index()
        {
            ViewBag.Mess = "Partial View from Index";
            return View();
        }

        public ActionResult GetList()
        {
            ViewBag.Mess = "Partial View";
            return PartialView("_GetList");
        }

        public ActionResult GetModel()
        {
            IEnumerable<Book> lst = new List<Book>()
            {
                new Book() { Name = "Игры разума", Author = "Shildt", Price = 200},
                new Book() { Name = "Игры разума", Author = "Shildt", Price = 200}
            };
            return PartialView("_GetModel", lst);
        }
    }
}