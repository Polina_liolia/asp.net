﻿using _007_HTTPContext_JQuery_Filters.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _007_HTTPContext_JQuery_Filters.Controllers
{
    public class ObjectsTransferController : Controller
    {
        // GET: ObjectsTransfer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Array()
        {

            return View();
        }

        [HttpPost]
        public string Array(List<string> names)
        {
            string fin = "";
            for (int i = 0; i < names.Count; i++)
            {
                fin += names[i] + ";  ";
            }
            return fin;
        }
        

        [HttpGet]
        public ActionResult GetAuthor()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetAuthor(Author author)
        {
            return View(); // поставим точку останова и просмотрим
        }


    }
}