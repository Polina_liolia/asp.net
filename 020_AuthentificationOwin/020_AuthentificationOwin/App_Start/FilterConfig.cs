﻿using System.Web;
using System.Web.Mvc;

namespace _020_AuthentificationOwin
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
