﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MyASP_test.Models
{
    public class BookStoreContext : DbContext  
    {
        public BookStoreContext() : base("name=LocalHome")
        {

        }
        public DbSet<Book> Books { get; set; }
        public DbSet<Purchase> Purchases { get; set; }

    }
}