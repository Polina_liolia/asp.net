﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MyASP_test.Controllers
{
    public class MyController : IController
    {
        //при реализации интерфейса IController мы имеем дело с одним методом Execute,
        //и все запросы к этому контроллеру, будут обрабатываться только одним методом
        public void Execute(RequestContext requestContext)
        {
            string ip = requestContext.HttpContext.Request.UserHostAddress;
            var response = requestContext.HttpContext.Response;
            response.Write("<h2>Ваш IP-адрес: " + ip + "</h2>");
        }
    }
}