﻿using MyASP_test.Models;
using MyASP_test.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyASP_test.Controllers
{
    public class HomeController : Controller
    {
        BookStoreContext context = new BookStoreContext();

        public ActionResult Index()
        {
            IEnumerable<Book> books = context.Books;
            //передача списка объектов Book в представление:
            ViewBag.Books = books; //список будет доступен в представлении через переменную Books

            return View();
        }

        [HttpGet]
        public ActionResult Buy(int id)
        {
            ViewBag.BookId = id;
            return View();
        }

        [HttpPost]
        public string Buy(Purchase purchase)
        {
            purchase.Date = DateTime.Now;
            context.Purchases.Add(purchase);
            context.SaveChanges();
            return $"{purchase.Person}, thank you for purchase!";
        }

        //Объект Request содержит коллекцию Params, которая хранит все параметры, переданные в запросы.
        public string Square()
        {
            int a = Int32.Parse(Request.Params["a"]);
            int h = Int32.Parse(Request.Params["h"]);
            double s = a * h / 2.0;
            return "<h2>Площадь треугольника с основанием " + a + " и высотой " + h + " равна " + s + "</h2>";
        }

        public ActionResult GetHtml ()
        {
            return new HtmlResult("Hello world!");
        }

        public ActionResult GetImage()
        {
            string path = "../Images/Janes_painting.png";
            return new ImageResult(path);
        }
    }
}