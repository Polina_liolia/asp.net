﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MyASP_test.Util
{
    public class HtmlResult : ActionResult
    {
        private string htmlCode;
        public HtmlResult(string html)
        {
            htmlCode = html;
        }
        public override void ExecuteResult(ControllerContext context)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html><html><head>");
            sb.Append("<title>Главная страница</title>");
            sb.Append("<meta charset=utf-8 />");
            sb.Append("</head> <body>");
            sb.Append(htmlCode);
            sb.Append("</body></html>");
            context.HttpContext.Response.Write(sb.ToString());
        }
    }
}