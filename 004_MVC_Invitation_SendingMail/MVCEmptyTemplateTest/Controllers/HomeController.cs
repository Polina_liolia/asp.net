﻿using MVCEmptyTemplateTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCEmptyTemplateTest.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        //public ActionResult Index()
        //{
        //    return View();
        //}

        //public string Index()
        //{
        //    return "Good morning!";
        //}

        //public HtmlString Index()
        //{
        //    return new HtmlString("<h1>Good morning!</h1>");
        //}

        //public ViewResult Index()
        //{
        //    return View();
        //}

        public ViewResult Index()
        {
            int hour = DateTime.Now.Hour;
            ViewBag.Greeting = hour < 12 ? "Good morning!" : hour < 18 ? "Good afternoon!" : "Good night!";
            return View();
        }

        [HttpGet]
        public ViewResult InviteForm()
        {
            return View();
        }

        [HttpPost]
        public ViewResult InviteForm(GuestResponse guest)
        {
            // Нужно отправить данные нового гостя no электронной почте 
            // организатору.
            //return View("Thanks", guest);
            if (ModelState.IsValid)
                return View("Thanks", guest);
            else
                // Обнаружена ошибка проверки достоверности
                return View();
        }
    }
}