namespace ASP_MVC_ClassTest.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Models;

    internal sealed class Configuration : DbMigrationsConfiguration<ItemsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ItemsContext context)
        {
            base.Seed(context);
            context.Items.Add(new Item() { Name = "green apple", Price = 25.5f, Quantity = 1 });
            context.Items.Add(new Item() { Name = "red apple", Price = 20.3f, Quantity = 2 });
            context.SaveChanges();
        }
    }
}
