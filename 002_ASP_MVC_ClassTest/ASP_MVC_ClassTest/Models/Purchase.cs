﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_MVC_ClassTest.Models
{
    public class Purchase
    {
        public int Id { get; set; }
        public string Person { get; set; }
        public string Address { get; set; }
        public int ItemId { get; set; }
        public DateTime Date { get; set; }

    }
}