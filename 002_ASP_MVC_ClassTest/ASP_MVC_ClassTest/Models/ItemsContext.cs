﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ASP_MVC_ClassTest.Models
{
    public class ItemsContext : DbContext 
    {
        public ItemsContext() : base("ItemsContext")
        {

        }

        public DbSet<Item> Items { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
    }
}