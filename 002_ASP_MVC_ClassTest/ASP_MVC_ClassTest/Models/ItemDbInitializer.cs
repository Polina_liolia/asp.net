﻿using ASP_MVC_ClassTest.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ASP_MVC_ClassTest.Models
{
    public class ItemDbInitializer : DropCreateDatabaseAlways<ItemsContext>
    {
        protected override void Seed(ItemsContext context)
        {
            base.Seed(context);
            context.Items.Add(new Item() { Name = "blue apple", Price = 30.25f, Quantity = 2 });
            context.Items.Add(new Item() { Name = "orange apple", Price = 31.4f, Quantity = 3 });
            context.SaveChanges();
        }
    }
}