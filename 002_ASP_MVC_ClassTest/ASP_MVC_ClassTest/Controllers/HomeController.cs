﻿using ASP_MVC_ClassTest.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP_MVC_ClassTest.Controllers
{
    public class HomeController : Controller, IDisposable
    {
        ItemsContext dBContext = new ItemsContext();
        public ActionResult Index()
        {
            dBContext.Items.Load();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ViewResult MyView()
        {

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            dBContext.Dispose();
        }
    }
}