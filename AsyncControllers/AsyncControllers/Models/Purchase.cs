namespace AsyncControllers
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Purchase
    {
        [Key]
        public int PurchaseId { get; set; }

        public string Person { get; set; }

        public string Address { get; set; }

        public int BookId { get; set; }

        public DateTime Date { get; set; }
    }
}
