﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewsTypes.Models;

namespace ViewsTypes.Controllers
{
    public class HomeController : Controller
    {
        BookStoreDataContext dataContext = new BookStoreDataContext();

        public ActionResult Index()
        {
            ViewBag.Books = dataContext.Books.ToList();
            return View();
        }


        [HttpGet]
        public ActionResult IndexAllBooks()
        {
            return View(dataContext.Books);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Create (Book book)
        {
            dataContext.Books.Add(book);
            dataContext.SaveChanges();
            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public ActionResult Delete(int id)
        {
            Book bookToRemove = dataContext.Books.Find(id);
            if (bookToRemove == null)
            {
                return HttpNotFound();
            }
            return View(bookToRemove);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Book bookToRemove = dataContext.Books.Find(id);
            if (bookToRemove == null)
            {
                return HttpNotFound();
            }
            dataContext.Books.Remove(bookToRemove);
            dataContext.SaveChanges();
            return RedirectToAction("Index");
        }

        //Remote arrt callback method:
        [HttpGet]
        public JsonResult CheckName(string name)
        {
            var result = !(name == "Название");
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}