﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ViewsTypes.Annotations
{
    public class MyAthorsAttribute : ValidationAttribute
    {
        private static string[] myAuthors;

        public MyAthorsAttribute(string[] authors)
        {
            myAuthors = authors;
        }

        public override bool IsValid(object value)
        {
            bool isValid = false;
            if(value is string)
            {
                string strValue = value as string;
                if (strValue != null)
                {
                    isValid = myAuthors.Contains(strValue);
                }
            }
            return isValid;
        }
    }
}