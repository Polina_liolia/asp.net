﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ViewsTypes.Models;

namespace ViewsTypes.Annotations
{
    public class NotAllowedAttribute : ValidationAttribute
    {
        public NotAllowedAttribute()
        {
            this.ErrorMessage = "Can not add such a book.";
        }
        public override bool IsValid(object value)
        {
            bool isValid = false;
            if (value is Book)
            {
                Book book = value as Book;
                if (book != null)
                {
                    if (book.Author != "AAA" && book.Name != "BBB")
                        isValid = true;
                }
            }
            return isValid;
        }
    }
}