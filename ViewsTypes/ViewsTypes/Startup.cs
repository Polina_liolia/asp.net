﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ViewsTypes.Startup))]
namespace ViewsTypes
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
