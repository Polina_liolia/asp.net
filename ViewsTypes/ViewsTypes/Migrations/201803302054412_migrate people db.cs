namespace ViewsTypes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migratepeopledb : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.People",
                p => new
                {
                    Id = p.Int(nullable: false, identity: true),
                    Name = p.String(nullable: false),
                    Email = p.String(nullable: false),
                    Password = p.String(nullable: false),
                    ConfirmPassword = p.String(nullable: false)
                });
        }
        
        public override void Down()
        {
            DropTable("dbo.People");
        }
    }
}
