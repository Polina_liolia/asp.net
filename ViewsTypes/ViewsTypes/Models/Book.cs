namespace ViewsTypes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Mvc;
    using ViewsTypes.Annotations;

    [NotAllowedAttribute()]
    public partial class Book
    {
        //[ScaffoldColumn(false)] //no hidden inputs will be generated at all
        [HiddenInput (DisplayValue =false)] //hidden <input> will be generated
        public int Id { get; set; }

        //������� Remote ��������� ��������� ��������� �� ������� ������� � ��������� �������� �� ������.
        [Required]
        [Display(Name="Book name")]
        [DataType(DataType.Text)]
        [Remote("CheckName", "Home", ErrorMessage = "Name is not valid.")]
        public string Name { get; set; }

        [Required(ErrorMessage="This field is required!")]
        [Display(Name="Author name")]
        [MyAthorsAttribute(new string[] { "Christie", "Pushkin"}, ErrorMessage ="No such author in the list!")]
        public string Author { get; set; }

        [Range(typeof(decimal), "0,00", "3000,99")]
        [Required]
        [Display(Name="Price, $")]
        [UIHint("Decimal")]
        public int Price { get; set; }
    }
}


/*
 * UIHint values:
 Boolean
������� �������������� ������� ������ (checkbox) ��� ������� ��������. ��� �������� ���� bool? (nullable) ��������� ������� select � ����������� True, False � Not Set
������� ����������� ���������� �� �� �������� html, ��� � ������� ��������������, ������ � ��������� disabled

Collection
������������ ��������������� ������ ��� ���������� ������� �������� ���������. ������ �������� ����� ���� ������ �����.

Decimal
������� �������������� ������� ������������ ��������� ���� - ������� input

EmailAddress
������� �������������� ������� ������������ ��������� ����.
������� ����������� ���������� ������� ������, ��� ������� href ����� �������� mailto:url

HiddenInput
��������� ������� ���� - ������� hidden input
Html
������� �������������� ������� ������������ ��������� ����.
������� ����������� ������ ���������� �����

MultilineText
������� �������������� ������� ������������� ��������� ���� (������� textarea)

Object
������� ������� �������� ������� � �������� �������� ���������� ��� ���� �������.

Password
������� �������������� ������� ��������� ���� ��� ����� �������� � �������������� �����
������� ����������� ���������� ������ ��� ����, ��� ������������� �����

String
������� �������������� ������� ������������ ��������� ����

Url
������� �������������� ������� ��������� ����
������� ����������� ������� ������� ������ ��� ������� Url
     */
