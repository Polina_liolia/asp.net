namespace ViewsTypes.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class BookStoreDataModel : DbContext
    {
        public BookStoreDataModel()
            : base("name=BookStoreDataModel")
        {
        }

        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<Purchase> Purchases { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
