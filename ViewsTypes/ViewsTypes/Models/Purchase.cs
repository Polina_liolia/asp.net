namespace ViewsTypes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    //������������� 
    public partial class Purchase : IValidatableObject
    {
        [Key]
        public int PurchaseId { get; set; }

        [StringLength(50, MinimumLength = 3, ErrorMessage = "����� ����� ������ ���� �� 3 �� 50 ��������")]
        public string Person { get; set; }

        public string Address { get; set; }

        public int BookId { get; set; }

        public DateTime Date { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if(Person.Length < 3 )
            {
                errors.Add(new ValidationResult("Person's name length must be more than 3 charachters!", new string[] { "Person" }));
            }
            if(!Address.Contains(" "))
            {
                errors.Add(new ValidationResult("Address must contain whitespaces", new string[] { "Address" }));
            }
            if(Date < DateTime.Parse("01/01/1990"))
            {
                errors.Add(new ValidationResult("Date must be grater than 01/01/1990", new string[] { "Date" }));
            }

            return errors;
        }
    }
}
