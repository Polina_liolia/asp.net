﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ViewsTypes.Models
{
    public class PersonDBContext : DbContext
    {
        public PersonDBContext() : base("PersonDB")
        {
        }

        public HashSet<Person> People { get; set; }

    }
}