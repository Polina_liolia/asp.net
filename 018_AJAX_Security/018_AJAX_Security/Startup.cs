﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(_018_AJAX_Security.Startup))]
namespace _018_AJAX_Security
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
