﻿using System.Web;
using System.Web.Mvc;

namespace _018_AJAX_Security
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
