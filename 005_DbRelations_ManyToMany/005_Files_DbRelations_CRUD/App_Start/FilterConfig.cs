﻿using System.Web;
using System.Web.Mvc;

namespace _005_Files_DbRelations_CRUD
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
