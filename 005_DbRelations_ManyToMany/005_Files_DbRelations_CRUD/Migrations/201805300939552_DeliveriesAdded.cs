namespace _005_Files_DbRelations_CRUD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeliveriesAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Deliveries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DeliveryParts",
                c => new
                    {
                        Delivery_Id = c.Int(nullable: false),
                        Part_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Delivery_Id, t.Part_Id })
                .ForeignKey("dbo.Deliveries", t => t.Delivery_Id, cascadeDelete: true)
                .ForeignKey("dbo.Parts", t => t.Part_Id, cascadeDelete: true)
                .Index(t => t.Delivery_Id)
                .Index(t => t.Part_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DeliveryParts", "Part_Id", "dbo.Parts");
            DropForeignKey("dbo.DeliveryParts", "Delivery_Id", "dbo.Deliveries");
            DropIndex("dbo.DeliveryParts", new[] { "Part_Id" });
            DropIndex("dbo.DeliveryParts", new[] { "Delivery_Id" });
            DropTable("dbo.DeliveryParts");
            DropTable("dbo.Deliveries");
        }
    }
}
