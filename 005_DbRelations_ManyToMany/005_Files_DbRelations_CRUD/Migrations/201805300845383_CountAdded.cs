namespace _005_Files_DbRelations_CRUD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CountAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("Parts", "Count", c => c.Int(nullable: true, defaultValue: 1));
        }
        
        public override void Down()
        {
            DropColumn("Parts", "Count");
        }
    }
}
