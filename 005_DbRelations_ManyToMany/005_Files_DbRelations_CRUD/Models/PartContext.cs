﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace _005_Files_DbRelations_CRUD.Models
{
    public class PartContext : DbContext
    {
        public PartContext() : base("PartContext")
        {
            
        }

        public DbSet<Part> Parts { get; set; }
        public DbSet<Delivery> Deliveries { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Delivery>()
                .HasMany(d => d.Parts)
                .WithMany(p => p.Deliveries);

            modelBuilder.Entity<Part>()
                .HasMany(p => p.Deliveries)
                .WithMany(d => d.Parts);

            base.OnModelCreating(modelBuilder);
            
        }
    }
}