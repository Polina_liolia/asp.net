﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace _005_Files_DbRelations_CRUD.Models
{
    public class Part : IDisposable
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [Display(Name="Имя")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Price is required")]
        [Display(Name = "Цена")]
        public double Price { get; set; }
        [Display(Name = "Описание")]
        public string Description { get; set; }
        [Display(Name = "Количество")]
        public int Count { get; set; }

        public void Dispose()
        {
            
        }

        [Display(Name = "Доставки")]
        public virtual ICollection<Delivery> Deliveries { get; set; }

        public Part()
        {
            Deliveries = new List<Delivery>();
        }
    }
}