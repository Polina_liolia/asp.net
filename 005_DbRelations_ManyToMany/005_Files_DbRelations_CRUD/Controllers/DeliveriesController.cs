﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using _005_Files_DbRelations_CRUD.Models;

namespace _005_Files_DbRelations_CRUD.Controllers
{
    public class DeliveriesController : Controller
    {
        private PartContext db = new PartContext();

        // GET: Deliveries
        public ActionResult Index()
        {
            return View(db.Deliveries.ToList());
        }

        // GET: Deliveries/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Delivery delivery = db.Deliveries.Find(id);
            if (delivery == null)
            {
                return HttpNotFound();
            }
            return View(delivery);
        }

        // GET: Deliveries/Create
        public ActionResult Create()
        {
            List<Part> parts = db.Parts.ToList<Part>();
            ViewBag.parts = parts;
            return View();
        }

        // POST: Deliveries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Delivery delivery, int[] selectedParts)
        {
            if (ModelState.IsValid)
            {
                if (selectedParts != null)
                {
                    foreach (int id in selectedParts)
                    {
                        Part part = db.Parts.Find(id);
                        if (part != null)
                        {
                            delivery.Parts.Add(part);
                        }
                    }
                   // db.Entry(delivery).State = EntityState.Modified;
                }
                db.Deliveries.Add(delivery);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(delivery);
        }

        // GET: Deliveries/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Delivery delivery = db.Deliveries.Find(id);
            if (delivery == null)
            {
                return HttpNotFound();
            }
            List<Part> parts = db.Parts.ToList<Part>();
            ViewBag.parts = parts;
            return View(delivery);
        }

        // POST: Deliveries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Delivery delivery, int[] selectedParts)
        {
            if (ModelState.IsValid)
            {
                Delivery currentDelivery = db.Deliveries.Find(delivery.Id);
                if(currentDelivery != null)
                {
                    currentDelivery.Address = delivery.Address;
                    currentDelivery.Parts.Clear();
                    foreach (int id in selectedParts)
                    {
                        Part part = db.Parts.Find(id);
                        if (part != null)
                        {
                            currentDelivery.Parts.Add(part);
                        }
                    }
                    db.Entry(currentDelivery).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                   
            }
            return View(delivery);
        }

        // GET: Deliveries/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Delivery delivery = db.Deliveries.Find(id);
            if (delivery == null)
            {
                return HttpNotFound();
            }
            return View(delivery);
        }

        // POST: Deliveries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Delivery delivery = db.Deliveries.Find(id);
            db.Deliveries.Remove(delivery);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
