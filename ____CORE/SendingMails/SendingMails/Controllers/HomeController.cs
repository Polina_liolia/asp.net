﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NETCore.MailKit.Core;
using SendingMails.Models;

namespace SendingMails.Controllers
{
    public class HomeController : Controller
    {
        private readonly Models.IEmailService _EmailService;

        public HomeController(Models.IEmailService emailService)
        {
            _EmailService = emailService;
        }

        public IActionResult Email()
        {
            ViewData["Message"] = "ASP.NET Core mvc send email example";

            EmailMessage msg = new EmailMessage("Sending mail test", "This mail was sent fron ASP Core application");
            msg.AddToAddress(new EmailAddress("polina.liolia@gmail.com", "Polina Liolia"));
            msg.AddFromAddress(new EmailAddress("polina.liolia@gmail.com", "Polina Liolia ASP Core"));
            _EmailService.Send(msg);

            return View();
        }


        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
