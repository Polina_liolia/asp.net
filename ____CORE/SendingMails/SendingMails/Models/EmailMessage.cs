﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SendingMails.Models
{
    public class EmailMessage
    {
        public EmailMessage(string subject, string content)
        {
            Subject = subject;
            Content = content;
            ToAddresses = new List<EmailAddress>();
            FromAddresses = new List<EmailAddress>();
        }

        public List<EmailAddress> ToAddresses { get; set; }
        public List<EmailAddress> FromAddresses { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }

        public void AddToAddress (EmailAddress toAddress)
        {
            ToAddresses.Add(toAddress);
        }

        public void AddFromAddress(EmailAddress fromAddress)
        {
            FromAddresses.Add(fromAddress);
        }

    }
}
