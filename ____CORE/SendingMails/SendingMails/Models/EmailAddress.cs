﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SendingMails.Models
{
    public class EmailAddress
    {
        public EmailAddress(string address, string name)
        {
            Address = address;
            Name = name;
        }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
