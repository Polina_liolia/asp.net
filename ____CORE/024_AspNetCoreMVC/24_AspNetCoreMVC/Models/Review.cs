﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreMVC.Models
{
    public class Review
    {
        public int Id { get; set; }
        public string Text { get; set; }

        public virtual User Student { get; set; }
        public virtual User Author { get; set; }

    }
}
