﻿using Microsoft.EntityFrameworkCore;

namespace ASPCoreMVC3.Models
{
    public class PhotoShotContext : DbContext
    {
        public DbSet<PhotoShot> PhotoShotes { get; set; }
        public DbSet<Order> Orders { get; set; }

        public PhotoShotContext() { }

        public PhotoShotContext(DbContextOptions<PhotoShotContext> options)
        : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
