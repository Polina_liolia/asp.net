﻿using System;
using System.Linq;

namespace ASPCoreMVC3.Models
{
    public static class InitData
    {
        public static void Initialize(PhotoShotContext context)
        {
            if (!context.PhotoShotes.Any())
            {
                context.PhotoShotes.AddRange(
                    new PhotoShot
                    {
                        PhotoShotDate = new DateTime(2018, 9, 1),
                        Place = "студия Svitlo",
                        Price = 1300.0f,
                        Description = "15 кадров в авторской ретуши",
                        Hours = 0.45f,

                    },
                      new PhotoShot
                      {
                          PhotoShotDate = new DateTime(2018, 9, 1),
                          Place = "ваша школа",
                          Price = 800.0f,
                          Description = "5 кадров в авторской ретуши",
                          Hours = 1.0f,

                      },
                         new PhotoShot
                         {
                             PhotoShotDate = new DateTime(2018, 9, 15),
                             Place = "городские локации",
                             Price = 1000.0f,
                             Description = "5 кадров в авторской ретуши",
                             Hours = 0.3f,

                         }
                );
                context.SaveChanges();
            }
        }
    }
}
