﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ASPCoreMVC3.Models
{
    public class Order
    {
        [ScaffoldColumn(false)]
        public int OrderId { get; set; }

        [Display(Name = "ФИО")]
        public string User { get; set; } // имя фамилия клиента

        [Display(Name = "Дата, время проведения")]
        public DateTime PhotoShotDateTime { get; set; }
        [Display(Name = "Телефон")]
        public string ContactPhone { get; set; } // контактный телефон 
        [Display(Name = "Предоплата")]
        public bool IsPrepayed { get; set; }

        public int PhotoShotId { get; set; } // ссылка на связанную модель Photo
        public PhotoShot PhotoShot { get; set; }
    }
}
