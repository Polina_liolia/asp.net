﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ASPCoreMVC3.Models
{
    public class PhotoShot
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Display(Name = "Дата проведения")]
        public DateTime PhotoShotDate { get; set; }

        [Display(Name = "Место проведения")]
        public string Place { get; set; }

        [Display(Name = "Стоимость участия")]
        public float Price { get; set; }

        [Display(Name = "Описание проекта")]
        public string Description { get; set; }

        [Display(Name = "Длительность")]
        public float Hours { get; set; }
    }
}
