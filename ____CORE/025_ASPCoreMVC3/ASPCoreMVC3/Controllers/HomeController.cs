﻿using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ASPCoreMVC3.Models;
using System;

namespace ASPCoreMVC3.Controllers
{
    public class HomeController : Controller
    {
        PhotoShotContext db;
        public HomeController(PhotoShotContext context)
        {
            db = context;
        }
        public IActionResult Index()
        {
            return View(db.PhotoShotes.ToList());
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        [HttpGet]
        public IActionResult Buy(int? Id)
        {
            ViewData["Message"] = "Buy.";
            ViewBag.photoShotId = Id;
            return View();
        }

        [HttpPost]
        public IActionResult Buy(Order order1)
        {
            ViewData["Message"] = "Buy.";
            Order order = new Order();
            order.User = order1.User;
            order.PhotoShotDateTime = order1.PhotoShotDateTime;
            order.ContactPhone = order1.ContactPhone;
            order.IsPrepayed = order1.IsPrepayed;
            order.PhotoShotId = order1.PhotoShotId;
            order.PhotoShot = db.PhotoShotes.Where(p => p.Id == order1.PhotoShotId).FirstOrDefault();
            db.Orders.Add(order);
            db.SaveChanges();
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
