﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MethodsUseMap.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MethodsUseMap
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //применяется для сопоставления пути запроса с определeнным делегатом, 
            //который будет обрабатывать запрос по этому пути
            app.Map("/index", Index);
            app.Map("/about", About);

            //вложенные методы Map, которые обрабатывают подмаршруты
            app.Map("/home", home =>
            {
                home.Map("/index", Index);
                home.Map("/about", About);
            });

            //принимает в качестве параметра делегат Func<HttpContext, bool> и обрабатывает запрос,
            //если функция, передаваемая в качестве параметра возвращает true
            app.MapWhen(context => {

                return context.Request.Query.ContainsKey("id") &&
                        context.Request.Query["id"] == "5";
            }, HandleId);

            //---------Конвейер обработки запроса---------
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage(); //Обработка ошибок
            }
            else
            {
                app.UseExceptionHandler("/Home/Error"); //Обработка ошибок
            }

            app.UseStaticFiles(); //Обработка запросов к статическим файлам

            //Политика работы с куки (GDPR)
            app.UseCookiePolicy();

            // Установка компонентов MVC для обработки запроса
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });


            //Метод Use также добавляет компоненты middleware, которые также обрабатывают запрос,
            //но в нем может быть вызван следующий в конвейере запроса компонент middleware
            int x = 3;
            int y = 10;
            int z = 0;
            app.Use(async (context, next) =>
            {
                z = x * y + z;
                //не рекомендуется вызывать метод next.Invoke после метода Response.WriteAsync()
                // await context.Response.WriteAsync($"x * y + z = {z}");
                await next.Invoke();
            });


            //добавление компонента middleware, который представляет класс, в конвейер обработки запроса
            app.UseMiddleware<TokenMiddleware>();

            // можно передать в метод расширения UseToken конкетное значение паттерна для сравнения
            app.UseToken("555555");


            // простейший способ для добавления компонентов middleware в конвейер. 
            //Однако компоненты, определенные через метод Run, не вызывают никакие другие компоненты
            //и дальше обработку запроса не передают.
            app.Run(async (context) =>  
            {
                await context.Response.WriteAsync("Hello World!");
            });
        }

        private void HandleId(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                await context.Response.WriteAsync("id is equal to 5");
            });
        }

        private void About(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                await context.Response.WriteAsync("About");
            });
        }

        private void Index(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                await context.Response.WriteAsync("Index");
            });
        }
    }
}
