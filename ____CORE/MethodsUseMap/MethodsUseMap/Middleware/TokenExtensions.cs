﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MethodsUseMap.Middleware
{
    public static class TokenExtensions
    {
        public static IApplicationBuilder UseToken(this IApplicationBuilder builder, string pattern)
        {
            //Здесь создается метод расширения для типа IApplicationBuilder. 
            //И этот метод встраивает компонент TokenMiddleware в конвейер обработки запроса. 
            return builder.UseMiddleware<TokenMiddleware>(pattern);
        }
    }
}
