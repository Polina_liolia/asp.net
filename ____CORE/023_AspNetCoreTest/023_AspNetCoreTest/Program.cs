﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace _023_AspNetCoreTest
{
    public class Program
    {
        public static void Main(string[] args)
        {
            using (var host = WebHost.Start("http://localhost:8080", async context =>
             {
                 context.Response.ContentType = "text/html; charset=utf-8";
                 await context.Response.WriteAsync("Hello world!");
             }))
            {
                host.WaitForShutdown();
            }
        }

        public static IWebHost BuildWebHost(string[] args) =>
        WebHost.CreateDefaultBuilder(args)
        .UseStartup<Startup>()
        .Build();


            //    // BuildWebHost(args).Run();
            //    var host = new WebHostBuilder()
            //        .UseKestrel()   //configures web server Kestrel
            //        .UseContentRoot(Directory.GetCurrentDirectory()) //configures root dir of project
            //        .UseIISIntegration()    //integration with IIS server
            //        .UseStartup<Startup>()  //sets main app file
            //        .Build();   //creates host
            //    host.Run(); //launches application

            //}

            //public static IWebHost BuildWebHost(string[] args) =>
            //    WebHost.CreateDefaultBuilder(args)
            //        .UseStartup<Startup>()
            //        .Build();
        }
    }
}
