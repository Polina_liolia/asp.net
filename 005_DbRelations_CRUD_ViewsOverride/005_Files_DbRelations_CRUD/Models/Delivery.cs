﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace _005_Files_DbRelations_CRUD.Models
{
    public class Delivery
    {
        public int Id { get; set; }
        public string Address { get; set; }

        public virtual ICollection<Part> Parts {get; set;}
        public Delivery()
        {
            Parts = new List<Part>();
        }

    }
}