namespace _005_Files_DbRelations_CRUD.Migrations
{
    using _005_Files_DbRelations_CRUD.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
   

    internal sealed class Configuration : DbMigrationsConfiguration<_005_Files_DbRelations_CRUD.Models.PartContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "_005_Files_DbRelations_CRUD.Models.PartContext";
        }

        protected override void Seed(_005_Files_DbRelations_CRUD.Models.PartContext context)
        {
            if(context.Parts.Count() == 0)
            {
                context.Parts.Add(new Part() { Name = "Part1", Price = 10.2, Count = 1, Description = "important part" });
                context.Parts.Add(new Part() { Name = "Part2", Price = 30.15, Count = 3, Description = "not wery important part" });
                context.Parts.Add(new Part() { Name = "Part3", Price = 45.28, Count = 5, Description = "some part" });
                context.SaveChanges();
            }
            
        }
    }
}
