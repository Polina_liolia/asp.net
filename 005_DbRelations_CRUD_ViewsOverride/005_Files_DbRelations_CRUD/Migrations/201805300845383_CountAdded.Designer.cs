// <auto-generated />
namespace _005_Files_DbRelations_CRUD.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class CountAdded : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CountAdded));
        
        string IMigrationMetadata.Id
        {
            get { return "201805300845383_CountAdded"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
