﻿using AJAX_usage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AJAX_usage.Controllers
{
    public class BooksController : Controller
    {
        BooksDbContext db = new BooksDbContext();
        // GET: Books
        public ActionResult Index()
        {
            return View(db.Books.ToList());
        }

        [HttpPost]
        public PartialViewResult BooksSearch(string author)
        {
            var booksFound = db.Books.Where(b => b.Author.Equals(author)).ToList();
            return PartialView(booksFound);
        }

        public PartialViewResult BestBook()
        {
            return PartialView(db.Books.First());
        }

        public JsonResult FirstBookJson()
        {
            Book firstBook = db.Books.First();
            return Json(firstBook, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult LastBookJson()
        {
            Book lastBook = db.Books.ToList().Last();
            return PartialView(lastBook);
        }
            

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }
    }
}