﻿using _016_ModelBinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _016_ModelBinding.Controllers
{
    public class HomeController : Controller
    {
        ItemContext db = new ItemContext();
        public ActionResult Index()
        {
            return View(db.Items);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Item book = db.Items.Find(id);
            if (book != null)
            {
                return View(book);
            }
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Edit(Item book)
        {

            Item oldBook = db.Items.Find(book.Id);
            if (oldBook != null)
            {
                oldBook.Name = book.Name;
                oldBook.Price = book.Price;
                oldBook.KDollar = book.KDollar;
                db.SaveChanges();
            }
            return HttpNotFound();
        }


        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Item book)
        {
            db.Items.Add(book);
            db.SaveChanges();

            return RedirectToAction("Index");
        }


        //[HttpPost]
        //public ActionResult Create([Bind(Include = "Name, Price")]Item book)
        //{
        //    db.Items.Add(book);
        //    db.SaveChanges();

        //    return RedirectToAction("Index");
        //}

        //[HttpPost]
        //public ActionResult Create([Bind(Exclude = "KDollar")]Item book)
        //{
        //    db.Items.Add(book);
        //    db.SaveChanges();

        //    return RedirectToAction("Index");
        //}

    }
}