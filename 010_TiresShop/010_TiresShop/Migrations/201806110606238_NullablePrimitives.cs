namespace _010_TiresShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullablePrimitives : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Addresses", "Person_Id", c => c.Int());
            AlterColumn("dbo.Contacts", "Person_Id", c => c.Int());
            AlterColumn("dbo.Orders", "PartialPaySum", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Orders", "PartialPaid", c => c.Boolean());
            AlterColumn("dbo.Orders", "TotalPaid", c => c.Boolean());
            AlterColumn("dbo.Tires", "Width", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Tires", "Profile", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Tires", "YearManufactured", c => c.Int());
            AlterColumn("dbo.Tires", "LoadIndex", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tires", "LoadIndex", c => c.Int(nullable: false));
            AlterColumn("dbo.Tires", "YearManufactured", c => c.Int(nullable: false));
            AlterColumn("dbo.Tires", "Profile", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Tires", "Width", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Orders", "TotalPaid", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Orders", "PartialPaid", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Orders", "PartialPaySum", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Contacts", "Person_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Addresses", "Person_Id", c => c.Int(nullable: false));
        }
    }
}
