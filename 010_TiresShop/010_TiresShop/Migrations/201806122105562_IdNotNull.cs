namespace _010_TiresShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdNotNull : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orders", "Client_Id", "dbo.Clients");
            DropPrimaryKey("dbo.People");
            DropPrimaryKey("dbo.Clients");
            AlterColumn("dbo.People", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Clients", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.People", "Id");
            AddPrimaryKey("dbo.Clients", "Id");
            AddForeignKey("dbo.Orders", "Client_Id", "dbo.Clients", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "Client_Id", "dbo.Clients");
            DropPrimaryKey("dbo.Clients");
            DropPrimaryKey("dbo.People");
            AlterColumn("dbo.Clients", "Id", c => c.Int(nullable: false));
            AlterColumn("dbo.People", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Clients", "Id");
            AddPrimaryKey("dbo.People", "Id");
            AddForeignKey("dbo.Orders", "Client_Id", "dbo.Clients", "Id");
        }
    }
}
