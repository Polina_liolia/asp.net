namespace _010_TiresShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TireAdditionalProperties : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tires", "CountryOrigin", c => c.String());
            AddColumn("dbo.Tires", "ModelName", c => c.String());
            AddColumn("dbo.Tires", "YearManufactured", c => c.Int(nullable: false));
            AddColumn("dbo.Tires", "SpeedIndex", c => c.String());
            AddColumn("dbo.Tires", "LoadIndex", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tires", "LoadIndex");
            DropColumn("dbo.Tires", "SpeedIndex");
            DropColumn("dbo.Tires", "YearManufactured");
            DropColumn("dbo.Tires", "ModelName");
            DropColumn("dbo.Tires", "CountryOrigin");
        }
    }
}
