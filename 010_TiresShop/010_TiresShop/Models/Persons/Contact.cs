﻿using _010_TiresShop.Models.Persons;
using System;
using System.ComponentModel.DataAnnotations;

namespace _010_TiresShop.Models
{
    public class Contact
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Value { get; set; }
        [Required]
        public virtual ContactType Type { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public virtual Client Person { get; set; }

    }
}