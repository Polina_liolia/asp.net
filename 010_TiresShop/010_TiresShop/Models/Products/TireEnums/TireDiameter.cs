﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _010_TiresShop.Models.Products.TireEnums
{
    public enum TireDiameter
    {
        R13, R14, R15, R15C, R16, R16C, R17, R17_5, R18, R19, R20, R21, R22, R22_5,
        R4, R5, R6, R8, R9, R10, R10C, R11, R12, R12C, R13C, R14C
    }
}