﻿using _010_TiresShop.Models.Products.TireEnums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _010_TiresShop.Models.Products
{
    public class Tire
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public decimal SelfCostUsd { get; set; }
        [Required]
        public decimal PriceUsd { get; set; }
        [Required]
        public decimal Discount { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        //specific properties:
        public decimal? Width { get; set; }
        public decimal? Profile { get; set; }
        [Required]
        public string BrandName { get; set; }
        [Required]
        public AutomobileType AutomobileType { get; set; }
        [Required]
        public TireDiameter Diameter { get; set; }
        [Required]
        public TireSeason Season { get; set; }
        public string CountryOrigin { get; set; }
        public string ModelName { get; set; }
        public int? YearManufactured { get; set; }
        public string SpeedIndex { get; set; }
        public int? LoadIndex { get; set; }



    }
}