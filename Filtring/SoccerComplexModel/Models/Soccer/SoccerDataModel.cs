namespace SoccerComplexModel.Models.Soccer
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using SoccerComplexModel.Models.Soccer.Configurations;

    public partial class SoccerDataModel : DbContext
    {
        public SoccerDataModel()
            : base("name=SoccerDataModel")
        {
        }

        public DbSet<Player> Players { get; set; }
        public DbSet<Team> Teams { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new PlayerConfigs());
            modelBuilder.Configurations.Add(new TeamConfigs());
        }
    }
}
