﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.ModelConfiguration;

namespace SoccerComplexModel.Models.Soccer.Configurations
{
    public class TeamConfigs : EntityTypeConfiguration<Team>
    {
        public TeamConfigs()
        {
            HasKey(t => t.Id);
            HasMany(t => t.Players).
                WithRequired(p => p.Team);
        }
    }
}