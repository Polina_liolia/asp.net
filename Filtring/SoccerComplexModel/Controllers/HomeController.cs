﻿using SoccerComplexModel.Models.Soccer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SoccerComplexModel.Controllers
{

    public class HomeController : Controller
    {
        
        SoccerDataModel db = new SoccerDataModel();

        public ActionResult Index(int? team, string position)
        {
            //var players = db.Players.ToList<Player>(); //no data from linked models will be loaded!! - no team info in players list
            IQueryable<Player> players = db.Players.Include("Team"); //navigation property specified
            if (team != null && team != 0) // team with id=0 -"All"
            {
                players = players.Where(p => p.TeamId == team);
            }
            if (!String.IsNullOrEmpty(position) && !position.Equals("All"))
            {
                players = players.Where(p => p.Position.Equals(position));
            }

            List<Team> teams = db.Teams.ToList<Team>();
            //adding "All" item:
            teams.Insert(0, new Team() { Name = "All", Id = 0 });

            List<string> positions = (from player in db.Players
                         select player.Position).Distinct().ToList();

            PlayersListViewModel plvm = new PlayersListViewModel()
            {
                Players = players,
                Teams = new SelectList(teams, "Id", "Name"),
                Positions = new SelectList(positions)
            };

            return View(plvm);
        }

        public ActionResult TeamDetails(int? id)
        {
            if (id == null)
                return HttpNotFound();
            Team team = db.Teams.Include("Players").FirstOrDefault(t => t.Id == id);
            if (team == null)
                return HttpNotFound();
            return View(team);
        }

        [HttpGet]
        public ActionResult Create()
        {
            // Формируем список команд для передачи в представление
            SelectList teams = new SelectList(db.Teams, "Id", "Name");
            ViewBag.Teams = teams;
            return View();
        }

        [HttpPost]
        public ActionResult Create(Player player)
        {
            //Добавляем игрока в таблицу
            db.Players.Add(player);
            db.SaveChanges();
            // перенаправляем на главную страницу
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            // Находим в бд футболиста
            Player player = db.Players.Find(id);
            if (player != null)
            {
                // Создаем список команд для передачи в представление
                SelectList teams = new SelectList(db.Teams, "Id", "Name", player.TeamId);
                ViewBag.Teams = teams;
                return View(player);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(Player player)
        {
            db.Entry(player).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}