﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Bundles_AJAX.Models
{
    class BookInitializer : DropCreateDatabaseAlways<BookContext>
    {
        protected override void Seed(BookContext context)
        {
            context.Books.Add(new Book() { Name = "C++", Author = "Дейтел" });
            context.Books.Add(new Book() { Name = "C#", Author = "Шилдт" });
            context.Books.Add(new Book() { Name = "F#", Author = "Дейтел" });
            context.Books.Add(new Book() { Name = "MS SQL", Author = "Шилдт" });
            base.Seed(context);
        }
    }
}