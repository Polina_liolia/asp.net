﻿using Bundles_AJAX.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bundles_AJAX.Controllers
{
    public class AjaxController : Controller
    {

        BookContext db = new BookContext();

        // GET: Ajax
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult BookSearch(string name)
        {
            var allbooks = db.Books.Where(a => a.Author.Contains(name)).ToList();
            if (allbooks.Count <= 0)
            {
                return HttpNotFound();
            }
            ViewBag.Authors = db.Books.Select(b => b.Author).Distinct();

            return View(allbooks);
        }
    }
}