﻿using Bundles_AJAX.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bundles_AJAX.Controllers
{
    public class HomeController : Controller
    {
        BookContext db = new BookContext();
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult BookSearch(string name)
        {
           
            var allbooks = db.Books.Where(a => a.Author.Contains(name)).ToList();
            if (allbooks.Count <= 0)
            {
                return HttpNotFound();
            }
            ViewBag.Authors = db.Books.Select(b => b.Author).Distinct();  

            return PartialView(allbooks);
        }

        public ActionResult BestBook()
        {
            Book book = db.Books.First();
            return PartialView(book);
        }

        public JsonResult JsonSearch(string name)
        {
            var jsondata = db.Books.Where(a => a.Author.Contains(name)).ToList<Book>();
            return Json(jsondata, JsonRequestBehavior.AllowGet);
        }
    }
}