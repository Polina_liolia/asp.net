﻿function OnSuccess(data) {
    var results = $('#results_json'); // получаем нужный элемент
    results.empty(); //очищаем элемент
    for (var i = 0; i < data.length; i++) {
        results.append('<li>' + data[i].Name + '</li>'); // добавляем данные в список
    }
}


$(document).ready(function () {
    $('#submit_search').click(function (e) {
        e.preventDefault();
        var name = $('#submit_search').val();
        name = encodeURIComponent(name);
        $('#jquery_ajax_result').load('@Url.Action("BookSearch", "Home")?name=' + name)
    });
});
