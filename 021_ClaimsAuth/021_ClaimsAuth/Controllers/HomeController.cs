﻿using _021_ClaimsAuth.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _021_ClaimsAuth.Controllers
{
    public class HomeController : Controller
    {
        [ClaimsAuthorize(Age = 16)]
        public ActionResult Index()
        {
            return View();
        }
        [ClaimsAuthorize(Age = 18)]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        [ClaimsAuthorize(Age = 21)]

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}