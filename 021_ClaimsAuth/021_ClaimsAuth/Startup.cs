﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(_021_ClaimsAuth.Startup))]
namespace _021_ClaimsAuth
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
