﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace _021_ClaimsAuth.Util
{
    public class ClaimsAuthorizeAttribute : AuthorizeAttribute
    {
        public int Age { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            ClaimsIdentity claimsIdentity;
            //имеет ли текущий пользователь доступ к ресурсe
            if (!httpContext.User.Identity.IsAuthenticated)
                return false;
            //с помощью получаемого контекста запроса httpContext извлекаем claim
            claimsIdentity = httpContext.User.Identity as ClaimsIdentity;
            var yearClaims = claimsIdentity.FindFirst("Year");
            if (yearClaims == null)
                return false;

            int year; // получаем год
            if (!Int32.TryParse(yearClaims.Value, out year))
                return false;

            // проверяем возраст относительно текущей даты
            if ((DateTime.Now.Year - year) < this.Age)
                return false;

            // обращаемся к методу базового класса
            return base.AuthorizeCore(httpContext);
        }
    }
}