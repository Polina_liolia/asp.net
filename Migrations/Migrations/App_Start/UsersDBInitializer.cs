﻿using Migrations.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Migrations.App_Start
{
    public class UsersDBInitializer : DropCreateDatabaseAlways<UserContext>
    {
        public override void InitializeDatabase(UserContext context)
        {
            base.InitializeDatabase(context);
        }

        protected override void Seed(UserContext context)
        {
            base.Seed(context);

            context.Users.Add(new User() { Name = "Ivan" });
            context.Users.Add(new User() { Name = "Petr" });
            context.Users.Add(new User() { Name = "Mary" });
            context.Users.Add(new User() { Name = "Alex" });
            context.Users.Add(new User() { Name = "John" });

            context.SaveChanges();
        }
    }
}