namespace Migrations.Migrations
{
    using global::Migrations.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<UserContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(UserContext context)
        {
            context.Companies.AddOrUpdate(new Company() { Name = "Apple" });
            context.Companies.AddOrUpdate(new Company() { Name = "Samsung" });
            context.Companies.AddOrUpdate(new Company() { Name = "LG" });

            context.Users.AddOrUpdate(new User() { Name = "Ivan" });
            context.Users.AddOrUpdate(new User() { Name = "Petr" });
            context.Users.AddOrUpdate(new User() { Name = "Mary" });
            context.Users.AddOrUpdate(new User() { Name = "Alex" });
            context.Users.AddOrUpdate(new User() { Name = "John" });

            context.SaveChanges();
        }
    }
}
