﻿using Migrations.App_Start;
using Migrations.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Migrations
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //IDatabaseInitializer<UserContext> userDBInitializer = new UsersDBInitializer();
            //using (UserContext db = new UserContext())
            //{
            //    userDBInitializer.InitializeDatabase(db);
            //}
        }
    }
}
